<?php

include "SCA/SCA.php";

$weather = SCA::getService('WeatherService.php');

$Areas = $weather->createDataObject('http://Weather',
                                      'Areas');
$area = $Areas->createDataObject('area');
$area->state = 'CA';
$area = $Areas->createDataObject('area');
$area->state = 'UT';
$area = $Areas->createDataObject('area');
$area->state = 'ND';

$Temperatures = $weather->getTemperature($Areas);

echo "Received temperatures from Web service:<br>";
foreach($Temperatures->entry as $Pair)
    echo $Pair->state . ": " . $Pair->temperature . "<br>";

?>
