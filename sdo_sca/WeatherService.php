<?php


include "SCA/SCA.php";

/**
 * @service
 * @binding.soap
 *
 * @types http://Weather AreasTypes.xsd
 * @types http://Weather TemperaturesTypes.xsd
 */
class WeatherService {

    /**
     * @param Areas $Areas http://Weather
     * @return Temperatures http://Weather
     */
    function getTemperature($Areas) {
        echo "Area list received from client:<br>";
        foreach($Areas->area as $area)
            echo $area->state . "<br>";
        echo "<br>";

        $Temperatures = SCA::createDataObject('http://Weather',
                                              'Temperatures');
        $Pair = $Temperatures->createDataObject('entry');
        $Pair->state = 'CA';
        $Pair->temperature = 65;
        $Pair = $Temperatures->createDataObject('entry');
        $Pair->state = 'UT';
        $Pair->temperature = 105;
        $Pair = $Temperatures->createDataObject('entry');
        $Pair->state = 'ND';
        $Pair->temperature = -20;
        return $Temperatures;
    }
}

?>
