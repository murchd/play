<h1>Cannon Hygiene Website Enquiry</h1>
<br />
<hr />

<h3>Message Details</h3>
<b>Subject</b><br />
#subject#<br />
<b>Message</b><br />
#message#<br />
<br />
<br />
<h3>Contact Details</h3>
Name: #name#<br />
Email: #email#<br />
Telephone: #telephone#<br />
Mobile: #mobile#<br />
Company: #company#<br />
Position: #position#<br />
<br />
<br />
<hr />

<p>Cannon Hygiene<br />
<small>Sent from website at: #baseurl#</small></p>
