<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template_generic.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Language" content="english" />    
    <meta name="author" content="Hairy Lemon" />
    <meta name="copyright" content="" />
	
	<link rel="stylesheet" type="text/css" href="css/styles_new.css" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
<!-- InstanceBeginEditable name="doctitle" -->
		<!-- InstanceEndEditable --><!-- InstanceBeginEditable name="head" --><title>Cannon Hygiene - Contact Us</title>
		<!-- InstanceEndEditable -->
</head>

<body>

<div id="main">
	<div id="header">
		<a href="/" title="Cannon Hygiene"><img src="images/spacer.gif" alt="Cannon Hygiene" /></a>
	</div>
	<div id="nav">
		<a href="/">Home</a> | <a href="services.html">Our Services</a> | <a href="about.html">About Cannon Hygiene</a> | <a href="news.html">News</a> | <a href="employment.html">Employment Opportunities</a> | <a href="contact.html">Contact Us</a>	</div>
	<div id="content">
		<div id="right">
			<h1><!-- InstanceBeginEditable name="heading" --> quick enquiry <!-- InstanceEndEditable --></h1>
			<!-- InstanceBeginEditable name="content" -->
									<p>
									<?php 
									//Form errors
									if($errors) {
										foreach($errors as $error => $error_message) {
											echo $error_message . '<br />';
										}
									}
									
									
									?>
									</p>
									<form name="form1" action="processEnquiry.php" method="post" runat="server" id="Form1">
										<table border="0" cellspacing="0" cellpadding="5" width="100" style="MARGIN-LEFT:10px">
											<tr>
												<td colspan="2"><b>Subject of your Enquiry:</b></td>
											</tr>
											<tr>
												<td colspan="2">
													<textarea runat="server" id="subject" cols="65" rows="2" class="form" name="subject"><?php echo $enquiryForm->getData('subject')?></textarea>
												</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="2"><b>Your Enquiry:</b></td>
											</tr>
											<tr>
												<td colspan="2">
													<textarea runat="server" id="enq" cols="65" rows="10" class="form" name="message"><?php echo $enquiryForm->getData('message')?></textarea></TEXTAREA></td>
											</tr>
											<tr>
												<td><b>Name:</b></td>
												<td>
													<input runat="server" class="form" id="name" type="text" size="50" name="name" value="<?php echo $enquiryForm->getData('name')?>" /></td>
											</tr>
											<tr>
												<td><b>Company:</b></td>
												<td>
													<input runat="server" id="company" type="text" class="form" size="50" name="company" value="<?php echo $enquiryForm->getData('company')?>" /></td>
											</tr>
											<tr>
												<td><b>Position:</b></td>
												<td>
													<input runat="server" id="position" type="text" class="form" size="50" name="position" value="<?php echo $enquiryForm->getData('position')?>" /></td>
											</tr>
											<tr>
												<td><b>E-mail:</b></td>
												<td>
													<input runat="server" id="email" type="text" class="form" size="50" name="email" value="<?php echo $enquiryForm->getData('email')?>" /></td>
											</tr>
											<tr>
												<td><b>Telephone:</b></td>
												<td>
													<input runat="server" id="telephone" type="text" class="form" size="50" name="telephone" value="<?php echo $enquiryForm->getData('telephone')?>" /></td>
											</tr>
											<tr>
												<td><b>Mobile:</b></td>
												<td>
													<input runat="server" id="mobile" type="text" class="form" size="50" name="mobile" value="<?php echo $enquiryForm->getData('mobile')?>" /></td>
											</tr>
											<tr>
												<td><b>Website:</b></td>
												<td>
													<input runat="server" id="website" type="text" class="form" size="50" name="website" value="<?php echo $enquiryForm->getData('website')?>" /></td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="2"><b>How did you find the Cannon Hygiene Website:</b></td>
											</tr>
											<tr>
												<td colspan="2">
													<textarea runat="server" id="findout" cols="65" rows="4" class="form"></textarea></td>
											</tr>
											<tr>
												<td colspan="2" align="right">
													<input type="hidden" name="15" value="1906374" /> <input type="hidden" name="stats_visitorID" value="1906374" /><br />
													<input runat="server" type="image" src="images/submit.gif" width="89" height="34" border="0"
														id="imageSubmit" value="Submit" /><br />
													<br />
													<br />
												</td>
											</tr>
										</table>
									</form>
									<!-- InstanceEndEditable -->
		</div>
		<div id="left">
		</div>
		<div id="clear"></div>
	</div>
	<div id="footer"><div id="hL">website <a href="http://www.hairylemon.co.nz" target="_blank">design</a> by Hairy lemon</a></div>"hygiene solutions for washroom - technology - linen"</div>
</div>

<div id="tagLine"><img src="images/beyond_excellence.gif" alt="beyond excellence" /></div>

</body>
<!-- InstanceEnd --></html>

