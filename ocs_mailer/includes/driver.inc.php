<?php

/**
 * @file driver.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Core system initialization code.
 * This file is loaded before any others.
 * Any system-wide imports or initialization code should be placed here. 
 *
 * @package includes
 *
 * $Id: driver.inc.php,v 1.10 2009/10/26 00:25:33 dwm77 Exp $
 */


/**
 * Basic initialization (pre-classloading).
 */

// Useful for debugging purposes -- may want to disable for release version?
//error_reporting(E_ALL);
ini_set('display_errors','On');

// Update include path
define('ENV_SEPARATOR', strtolower(substr(PHP_OS, 0, 3)) == 'win' ? ';' : ':');
if (!defined('DIRECTORY_SEPARATOR')) {
	// Older versions of PHP do not define this
	define('DIRECTORY_SEPARATOR', strtolower(substr(PHP_OS, 0, 3)) == 'win' ? '\\' : '/');
}
define('CORE_SYS_DIR', '../core_framework');
define('BASE_SYS_DIR', dirname(dirname(__FILE__)));
ini_set('include_path', '.'
	. ENV_SEPARATOR . BASE_SYS_DIR . '/includes'
	. ENV_SEPARATOR . BASE_SYS_DIR . '/classes'	
	. ENV_SEPARATOR . BASE_SYS_DIR . '/pages'
	. ENV_SEPARATOR . BASE_SYS_DIR . '/lib'
	. ENV_SEPARATOR . CORE_SYS_DIR . '/classes'
	. ENV_SEPARATOR . CORE_SYS_DIR . '/'
	. ENV_SEPARATOR . CORE_SYS_DIR . '/lib'
	. ENV_SEPARATOR . CORE_SYS_DIR . '/lib/smarty'	
	. ENV_SEPARATOR . ini_get('include_path')
);


// Seed random number generator
mt_srand(((double) microtime()) * 1000000);

// System-wide functions
require('functions.inc.php');


/**
 * System class imports.
 * Only classes used system-wide should be included here.
 */

import('core.Core');
import('core.Request');
import('core.String');
import('config.Config');
import('mail.Email');



/**
 * System initialization (post-classloading).
 */

// Initialize string wrapper library
String::init();


?>
