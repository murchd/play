<?php
	require_once 'includes/driver.inc.php';
	import('enquiry.form.EnquiryForm');
	$enquiryForm = new EnquiryForm();
	$enquiryForm->initData();
	$enquiryForm->readInputData();
	if($enquiryForm->validate()) {
		$enquiryForm->execute();
		//FIXME make sure this is right
		header("Location: thankyou.html");
	}else{
		$errors = $enquiryForm->getErrorsArray();
		include('enquiryForm.php');
	}

?>