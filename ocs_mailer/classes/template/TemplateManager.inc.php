<?php
/***************************************
// TEMPLATE_ENGINE
****************************************/
class TemplateManager {
   private $template;
   public function display()
   {
      echo $this->template;
   }
   
   public function fetch() {

		return $this->template;
   }
   public function load_str($str)
   {
      $this->template = $str;
   }
   public function assign($var, $content) {

      $this->template = str_replace("#$var#", $content, $this->template);
   }
   public function load($filepath) {
		$this->template = file_get_contents($filepath);;
   }
}
?>