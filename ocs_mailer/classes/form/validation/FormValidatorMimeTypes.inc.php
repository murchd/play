<?php
/**
 * Checks an uploaded files' mime type against array
 * of passed in accepted types
 */


import('form.validation.FormValidator');

class FormValidatorMimeTypes extends FormValidator {

	/**  Array of all values accepted as valid */
	var $acceptedValues;
	
	/**
	 * Constructor.
	 * @see FormValidator::FormValidator()
	 * @param $acceptedValues array all possible accepted values
	 */
	function FormValidatorMimeTypes(&$form, $field, $message,$acceptedMimeTypes) {
		$type = "optional";
		parent::FormValidator($form, $field, $type, $message);
		$this->acceptedValues = $acceptedMimeTypes;
	}
	
	/**
	 * Check if field value is valid.
	 * Value is valid if it is empty and optional or is in the set of accepted values.
	 * @return boolean
	 */
	function isValid() {
		if(FileManager::uploadedFileExists($this->field)) {
			if(in_array(FileManager::getUploadedFileType($this->field), $this->acceptedValues)) {
				return True;
			}else{
				return False;
			}
		}else{
			return true;
		}
	}
	
}

?>
