<?php

import('form.validation.FormValidatorRegExp');

class FormValidatorNumeric extends FormValidatorRegExp {

	function FormValidatorNumeric(&$form, $field, $type, $message) {
		parent::FormValidatorRegExp($form, $field, $type, $message,
			'/^[0-9]*$/i'
		);
	}
	
}

?>