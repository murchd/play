<?php
import('form.Form');
import('template.TemplateManager');
class EnquiryForm extends Form {
	
	
	function EnquiryForm() {
		parent::Form();
		
		$this->addCheck(new FormValidator($this, 'subject', 'required', 'Please provide a subject'));
		$this->addCheck(new FormValidator($this, 'message', 'required', 'Please enter your message'));
		$this->addCheck(new FormValidator($this, 'name', 'required', 'Please enter your name'));
		$this->addCheck(new FormValidatorEmail($this, 'email', 'required', 'Please enter your email address'));
	}
	
	function display() {
		
	}
	function initData() {
		
	}
	
	function getParameterNames() {
		$parameterNames = array('subject',
								'message',
								'name',
								'email',
								'telephone',
								'mobile',
								'website',
								'company',
								'position');

		return $parameterNames;
	}
	
	function readInputData() {
		$this->readUserVars($this->getParameterNames());
	}
	
	function validate() {
		return parent::validate();
	}
	function execute() {
		//TODO do the actual sending of the email
		
		$emailTemplateMgr = new TemplateManager();
		$emailTemplateMgr->load('templates/admin.tpl');
		$emailTemplateMgr->assign('name',$this->getData('name'));
		$emailTemplateMgr->assign('subject',$this->getData('subject'));
		$emailTemplateMgr->assign('message',nl2br($this->getData('message')));
		$emailTemplateMgr->assign('email',$this->getData('email'));
		$emailTemplateMgr->assign('telephone',$this->getData('telephone'));
		$emailTemplateMgr->assign('mobile',$this->getData('mobile'));
		$emailTemplateMgr->assign('website',$this->getData('website'));
		$emailTemplateMgr->assign('company',$this->getData('company'));
		$emailTemplateMgr->assign('position',$this->getData('position'));
		$emailTemplateMgr->assign('baseurl',Request::getBaseUrl());
		$body = $emailTemplateMgr->fetch();

		$email = new Email();
		$email->setSubject(Config::getVar('email','subject'));
		$email->setBody($body);
		$email->send();
		
		
		
	}
	
	
	
	
}

?>