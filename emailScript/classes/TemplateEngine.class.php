<?php
/***************************************
// TEMPLATE_ENGINE
****************************************/
class TemplateEngine {
   var $template;
   function load($filepath)
   {
      $this->template = file_get_contents($filepath);
   }
     function load_str($str)
   {
      $this->template = $str;
   }
   function replace($var, $content) {

      $this->template = str_replace("#$var#", $content, $this->template);
   }
   function publish() {
		return $this->template;
   }
}
?>