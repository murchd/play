<?php
/**
 * Main Line program. Handles the form submit and create new email and sends
 */
require_once 'classes/Email.class.php';
require_once 'classes/TemplateEngine.class.php';
require_once 'config.inc.php';
require_once 'include/functions.inc.php';

if(array_key_exists('theEmail', $_POST)) {

	// Filter out HTML code as this could be malisious
	$theMsg = htmlspecialchars($_POST['theMsg']);
	$theName = htmlspecialchars($_POST['theName']);
	$theEmail = htmlspecialchars($_POST['theEmail']);
	$theWord = htmlspecialchars($_POST['theWord']);
	
	$error = validateContactForm($theMsg,$theName,$theEmail,$theWord);
	
	if(!$error) {
		// create new instance of email templateEngine
		$templateMgr = new TemplateEngine();
		$templateMgr->load('email_template.tpl');
		$templateMgr->replace('theMsg',$theMsg);
		$templateMgr->replace('theName',$theName);
		$templateMgr->replace('theEmail',$theEmail);
		$theEmailBody = $templateMgr->publish();
		
		
		// create new instance of email
		$theEmail = new Email();
		$theEmail->thefromAddress = $thefromAddress;
		$theEmail->thefromName = $thefromName;
		$theEmail->subject = $subject; 
		$theEmail->sendToAddress = $sendToAddress; 
		$theEmail->body = $theEmailBody;
		if($theEmail->send()) {
			$success = true;
		} else {
			$success = false;
		}
	}
}

//Display the HTML form
include 'form.php';
















?>