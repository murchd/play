<?php
if($success) {

	// Display success msg
	echo 'The form has been submitted successfully. Your email has been sent.';

} else {
	// Display the form
	?>
	<?php
	if($error){
		echo '<fieldset><legend class="formerror"><strong>An error has occured</strong></legend>';
		foreach($error as $value) {
			echo $value.'<br>';
		}
		echo '</fieldset>';
	}
	?>
<form action="<?=$PHP_SELF?>" method="POST" name="form1">
<table width="500" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2"><strong>Use this form to send an email to us </strong></td>
	</tr>
	<tr>
		<td>Your Message</td>
		<td><textarea name="theMsg" cols="35" rows="6"><?=$theMsg?></textarea></td>
	</tr>
	<tr>
		<td>Your Name</td>
		<td><input type="text" name="theName" value="<?=$theName?>" /></td>
	</tr>
	<tr>
		<td>Your Email</td>
		<td><input name="theEmail" type="text" size="30" maxlength="255"
			value="<?=$theEmail?>" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"><font>Security Check (enter letters that appear in
		image below)</font></td>
	</tr>
	<tr>
		<td>(This is done to reduce scripted abuse of this form)</td>
		<td><img src="images/.4525232534fish.jpg" border="0"></td>
	</tr>
	<tr>
		<td>Word above *</td>
		<td><input name="theWord" type="text" size="12" maxlength="255" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>* Required</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input name="submit" type="submit" value="Send the Email >>" /></td>
	</tr>
</table>
</form>
	<?php
}
?>
