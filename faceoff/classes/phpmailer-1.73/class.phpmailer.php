<?php
require_once('Email.inc.php');
class PHPMailer {
	var $From;
	var $FromName;
	var $WordWrap;
	var $Subject;
	var $Body;
	var $addresses = array();
	private $Email;
	
	function PHPMailer() {
		$this->Email = new Email();
	}
	
	function IsMail() {
		return;
	}
	
	function AddAddress($address) {
		$this->addresses[] = $address;
	}
	
	function IsHTML($flag) {
		if($flag) {
			$this->Email->setContentType('text/html');
		}else{
			$this->Email->setContentType('text/plain');
		}
	}
	
	function Send() {
		$this->setUpEmail();
		foreach($this->addresses as $receiprent) {
			$this->Email->setRecipientAddress($receiprent);
			$this->Email->send();
		}
	}
	
	function setUpEmail() {
		$this->Email->setSenderAddress($this->From);
		$this->Email->setSenderName($this->FromName);
		$this->Email->setSubject($this->Subject);
		$this->Email->setBody($this->Body);
	}
	
}

?>