<?php

require_once("classes/phpmailer-1.73/class.phpmailer.php");
function send_mail() {	
	$mail = new PHPMailer();
	$mail->IsMail();  
	
	$mail->AddAddress('david@wetstone.co.nz');  
	
	$mail->From     = 'noreply@wetstone.co.nz';
	$mail->FromName = 'Face off Test';
	
	// set word wrap
	$mail->WordWrap = 50;                              
	
	// send as HTML
	$mail->IsHTML(true);                              
	
	$mail->Subject  =  'test subject';
	$mail->Body     =  '<b>test body</b>';
	
	if(!$mail->Send())
	{
	   return false;
	}
	else
	{
	   return true;
	}
}

if(send_mail()) {
	echo "Email sent";
}else{
	echo "Email failed";
}


?>