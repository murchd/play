<?
echo "posix_getpid()=".posix_getpid().", posix_getppid()=".posix_getppid()."\r\n \r\n";

$pid = pcntl_fork();
if ($pid == -1) die("could not fork");
if ($pid) {
    echo "PARENT pid=".$pid.", posix_getpid()=".posix_getpid().", posix_getppid()=".posix_getppid()."\n";
} else {
    echo "CHILD pid=".$pid.", posix_getpid()=".posix_getpid().", posix_getppid()=".posix_getppid()."\n";
}

register_shutdown_function('handleShutdown');

function handleShutdown() {
        $error = error_get_last();
        if($error !== NULL){
            $info = "[SHUTDOWN] file:".$error['file']." | ln:".$error['line']." | msg:".$error['message'] .PHP_EOL;
            yourPrintOrMailFunction($info);
        }
        else{
            yourPrintOrMailFunction("SHUTDOWN");
        }
}
?>