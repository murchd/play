;<?php exit(); ?>
;DONT DELETE THE LINE ABOVE


;;;;;;;;;;;;;;;;;;;;
; General Settings ;
;;;;;;;;;;;;;;;;;;;;

[general]
installed = On

base_url = "http://localhost"

registry_dir = registry

session_cookie_name = wsapp
session_lifetime = 30

; Short and long date formats
date_format_trunc = "%m-%d"
date_format_short = "%Y-%m-%d"
date_format_long = "%B %e, %Y"
datetime_format_short = "%Y-%m-%d %I:%M %p"
datetime_format_long = "%B %e, %Y - %I:%M %p"

; Use URL parameters instead of CGI PATH_INFO. This is useful for
; broken server setups that don't support the PATH_INFO environment
; variable.
disable_path_info = Off

allow_url_fopen = Off



;;;;;;;;;;;;;;;;;;
; Email Settings ;
;;;;;;;;;;;;;;;;;;
[email]

fromname = "My Website"
fromaddress = noreply@rhyschapman.com
sendtoaddress = david@davidmurch.com


;;;;;;;;;;;;;;;;;;
; Cache Settings ;
;;;;;;;;;;;;;;;;;;

[cache]


cache = file

memcache_hostname = localhost
memcache_port = 11211

;;;;;;;;;;;;;;;;;;;;;;;;;
; Localization Settings ;
;;;;;;;;;;;;;;;;;;;;;;;;;

[i18n]

locale = en_US

client_charset = utf-8

connection_charset = Off

database_charset = Off


