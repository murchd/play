<?php

/**
 * @file DataObject.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package core
 * @class DataObject
 *
 * Abstract class for data objects. 
 * Any class with an associated DAO should extend this class.
 *
 * $Id: DataObject.inc.php,v 1.1 2009/11/16 20:34:37 dwm77 Exp $
 */

class DataObject {

	/** Array of object data */
	var $_data;
	
	/** Fields modified since object creation */
	/* var $_modified_data; */
	
	/**
	 * Constructor.
	 */
	function DataObject($callHooks = true) {
		$this->_data = array();
		/*
		$this->_modified_data = array();
		*/
	}
	
	/**
	 * Get the value of a data variable.
	 * @param $key string
	 * @return mixed
	 */
	function &getData($key) {
		if (isset($this->_data[$key])) {
			return $this->_data[$key];
		}
		$nullVar = null;
		return $nullVar;
	}
	
	/**
	 * Set the value of a new or existing data variable.
	 * @param $key string
	 * @param $value mixed
	 */
	function setData($key, $value) {
		$this->_data[$key] = $value;
		/*
		if (!in_array($key, $this->_modified_data))
		{
			$this->_modified_data[] = $key;
		}
		*/
	}
	function toXML(){		
		$xmlReturn = "<data dateTime='".Core::getCurrentDate()."'>";
		foreach ($this->_data as $key => $value){	
			if(is_array($value)){
				$xmlReturn .= "<".$key.">".$value->toXML()."</".$key.">";
			}else{
				$xmlReturn .= "<".$key.">".$value."</".$key.">";			
			}
		}
		$xmlReturn .= "</data>";
		return $xmlReturn; 
	}
	
	function toObject(){
		$tempObject = $this->arr2obj($this->_data);
		return $tempObject;		
	}
	
	
	function arr2obj($arg_array) {
		$tmp = new stdClass; // start off a new (empty) object
		foreach ($arg_array as $key => $value) {
			if (is_array($value)) { // if its multi-dimentional, keep going :)
				//$tmp->$key = $this->arr2obj($value);
			} else {
				if (is_numeric($key)) { // can't do it with numbers :(
					die("Cannot turn numeric arrays into objects!");
				}
				$tmp->$key = $value;
			}
		}
		return $tmp; // return the object!
	}
	
	
}

?>
