<?php

/**
 * @file Handler.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package core
 * @class Handler
 *
 * Base request handler class.
 *
 * $Id: Handler.inc.php,v 1.1 2009/11/16 20:34:37 dwm77 Exp $
 */

class Handler {

	/**
	 * Fallback method in case request handler does not implement index method.
	 */
	function index() {
		header('HTTP/1.0 404 Not Found');
		fatalError('404 Not Found');
	}
	
	/**
	 * Perform request access validation based on security settings.
	 */
	function validate() {
		if (Config::getVar('security', 'force_ssl') && Request::getProtocol() != 'https') {
			// Force SSL connections site-wide
			Request::redirectSSL();
		}
	}

	/**
	 * Return the DBResultRange structure and misc. variables describing the current page of a set of pages.
	 * @param $rangeName string Symbolic name of range of pages; must match the Smarty {page_list ...} name.
	 * @return array ($pageNum, $dbResultRange)
	 */
	function &getRangeInfo($rangeName,$count = null) {
		$pageNum = Request::getUserVar($rangeName . 'Page');
		if(empty($pageNum)) {
			$pageNum=1;
		}
		if(!$count) {
			$count = Config::getVar('interface', 'items_per_page');
		}
		import('db.DBResultRange');

		if(isset($count)) {
			$returner = &new DBResultRange($count, $pageNum);
		}else{
			$returner = &new DBResultRange(-1, -1);
		}

		return $returner;
	}
	
	function redirectMessage($msg,$redirectUrl = array(),$msgOp = 'index') {
				Request::redirect('message',
				 $msgOp,
				  null,
				   array('page' => $redirectUrl[0],
				   		'op' => $redirectUrl[1],
				   		 'message' => $msg)
				);
	}
}
?>