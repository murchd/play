<?php

/**
 * @file ArrayResultFactory.inc.php
 *
 *
 * @package array
 * @class ArrayResultFactory
 *
 * Provides paging and iteration for arrays of Standard Objects from array call.
 *
 * $Id: ArrayResultFactory.inc.php,v 1.1 2009/11/16 20:34:37 dwm77 Exp $
 */

import('core.ArrayItemIterator');

class ArrayResultFactory extends ArrayItemIterator {
	/** The DAO used to create objects */
	var $dao;

	/** The name of the DAO's factory function (to be called with a Standard Object) */
	var $functionName;

	/**
	 * Constructor.
	 * @param $theArray array The array of items to iterate through
	 * @param $page int the current page number
	 * @param $itemsPerPage int Number of items to display per page
	 */
	function ArrayResultFactory(&$theArray, &$dao, $functionName, $page=-1, $itemsPerPage=-1) {
		$this->functionName = $functionName;
		$this->dao = &$dao;
		
		if ($page>=1 && $itemsPerPage>=1) {
			$this->theArray = $this->array_slice_key($theArray, ($page-1) * $itemsPerPage, $itemsPerPage);
			$this->page = $page;
		} else {
			$this->theArray = &$theArray;
			$this->page = 1;
			$this->itemsPerPage = max(count($this->theArray),1);
		}
		$this->count = count($theArray);
		$this->itemsPerPage = $itemsPerPage;
		$this->wasEmpty = count($this->theArray)==0;
		reset($this->theArray);
	}

	/**
	 * Return the next item in the iterator.
	 * @return object
	 */
	function &next() {
		$value = &current($this->theArray);
		$functionName = &$this->functionName;
		$dao = &$this->dao;
		$result = &$dao->$functionName($value);
		if (next($this->theArray)==null) {
			$this->theArray = null;
		}
		return $result;
	}
}

?>