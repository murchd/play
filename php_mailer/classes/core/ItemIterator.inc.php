<?php

/**
 * @file ItemIterator.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package core
 * @class ItemIterator
 *
 * Generic iterator class; needs to be overloaded by subclasses
 * providing specific implementations.
 *
 * $Id: ItemIterator.inc.php,v 1.1 2009/11/16 20:34:37 dwm77 Exp $
 */

class ItemIterator {
	/**
	 * Return the next item in the iterator.
	 * @return object
	 */
	function &next() {
		return null;
	}

	/**
	 * Return the next item with key.
	 * @return array ($key, $value);
	function &nextWithKey() {
		return array(null, null);
	}

	function atFirstPage() {
		return true;
	}

	function atLastPage() {
		return true;
	}

	function getPage() {
		return 1;
	}

	function getCount() {
		return 0;
	}

	function getPageCount() {
		return 0;
	}

	/**
	 * Return a boolean indicating whether or not we've reached the end of results
	 * @return boolean
	 */
	function eof() {
		return true;
	}

	/**
	 * Return a boolean indicating whether or not this iterator was empty from the beginning
	 * @return boolean
	 */
	function wasEmpty() {
		return true;
	}

	function &toArray() {
		return array();
	}
}

?>
