<?php
import('form.validation.FormValidator');

class FormValidatorMatchPassword extends FormValidator {
	
	function FormValidatorMatchPassword(&$form, $field, $type, $message){
		$this->confirmField = $field[1];
		parent::FormValidator($form, $field[0], $type, $message);	
	}
	
	function isValid() {
		if($this->form->getData($this->field) === $this->form->getData($this->confirmField)) {
			return true;
		}else{
			return false;
		}
	}
	

}

?>