<?php

/**
 * @file FormValidatorAlphaNum.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package form.validation
 * @class FormValidatorAlphaNum
 *
 * Form validation check for alphanumeric (plus interior dash/underscore) characters only.
 *
 * $Id: FormValidatorAlphaNum.inc.php,v 1.1 2009/11/16 20:34:39 dwm77 Exp $
 */

import('form.validation.FormValidatorRegExp');

class FormValidatorAlphaNum extends FormValidatorRegExp {

	/**
	 * Constructor.
	 * @see FormValidatorRegExp::FormValidatorRegExp()
	 */
	function FormValidatorAlphaNum(&$form, $field, $type, $message) {
		parent::FormValidatorRegExp($form, $field, $type, $message,
			'/^[A-Z0-9]+([\-_][A-Z0-9]+)*$/i'
		);
	}
	
}

?>
