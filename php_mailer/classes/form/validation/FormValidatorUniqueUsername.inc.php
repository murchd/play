<?php
import('form.validation.FormValidator');

class FormValidatorUniqueUsername extends FormValidator {
	
	function FormValidatorUniqueUsername(&$form, $field, $type, $message){
		parent::FormValidator($form, $field, $type, $message);	
	}
	
	function isValid() {
		$userDao =& CoreDAORegistry::getDAO('UserDAO');
		$user = $userDao->getUser($this->form->getData($this->field));
		if($user) {
			return false;
		}else{
			return true;
		}
	}
	

}
?>