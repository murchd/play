<?php

import('form.Form');
import('contactus.Contactus');
class ContactusForm extends Form {
				
	/** The ID of the contactus being edited */
	var $contactusId;	

	
	
	/** The contactus object */
	var $contactus;
	
	
	function ContactusForm($contactusId = null){
		parent::Form('contactus/form.tpl');
		
		$this->contactusId = isset($contactusId) ? $contactusId : null;	
		
		$this->addCheck(new FormValidator($this,'email','required','form.email.required'));
		$this->addCheck(new FormValidator($this,'subject','required','form.subject.required'));
		$this->addCheck(new FormValidator($this,'message','required','form.message.required'));
		
		if ($contactusId) {
			
		}
		
	}
	
	function display() {
		
		
		parent::display();
	}
	
	function initData() {
		if (isset($this->contactus)) {			
			$this->_data = array(
							'contactusId' => $this->contactus->getContactusId(),
							'email' => $this->contactus->getEmail(),
							'subject' => $this->contactus->getSubject(),
							'captcha' => $this->contactus->getCaptcha(),
							'message' => $this->contactus->getMessage()
										);
		}	else{
			$this->_data = array(
							'rand_string' => rand(1000,9999));
		}
	}
	
	function getParameterNames() {
		$parameterNames = array(
							'email',
							'subject',
							'captcha',
							'message',
							'contactusId'
								);

		return $parameterNames;
	}
	
	function readInputData() {
		$this->readUserVars($this->getParameterNames());
	}
	
	function validate() {
		$verif_box = $this->getData('captcha');
		if(md5($verif_box).'aefft4g' == $_SESSION['tntcon']){
			$_SESSION['tntcon'] = '';
		}else{
			$this->addError('captcha','form.captcha.invalid');
		}
	
		return parent::validate();
	}
	
	function execute() {
		
		if (!isset($this->contactus)) {
			$this->contactus = &new Contactus();
		}
		$this->contactus->setEmail($this->getData('email'));
		$this->contactus->setSubject($this->getData('subject'));
		$this->contactus->setCaptcha($this->getData('captcha'));
		$this->contactus->setMessage($this->getData('message')); 
		
		
	}
}

?>