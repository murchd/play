<?php

class Contactus extends DataObject {
				
	
	function setContactusId($contactusId){
		$this->setData('contactusId',$contactusId);
	}
	function getContactusId(){
		return $this->getData('contactusId');
	}
		
	function setEmail($email){
		$this->setData('email',$email);
	}				
	function getEmail(){
		return $this->getData('email');
		
	}
		
	function setSubject($subject){
		$this->setData('subject',$subject);
	}				
	function getSubject(){
		return $this->getData('subject');
		
	}
		
	function setCaptcha($captcha){
		$this->setData('captcha',$captcha);
	}				
	function getCaptcha(){
		return $this->getData('captcha');
		
	}
		
	function setMessage($message){
		$this->setData('message',$message);
	}				
	function getMessage(){
		return $this->getData('message');
		
	}

}

?>