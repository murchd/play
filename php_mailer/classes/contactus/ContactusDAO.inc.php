<?php

import ('contactus.Contactus');
		
class ContactusDAO extends DAO {
				
		
		/** Cached array of users by userName */
		var $contactusCache;

		/**
		* Constructor.
		*/
		function ContactusDAO() {
			parent::DAO();
			$this->contactusCache = array();

		}
	

	
		function &getContactusById($contactusId){
			// First check the in-memory response cache
			if (isset($this->contactusCache[$contactusId])) {
				return $this->contactusCache[$contactusId];
			}
			
			$result = &$this->retrieve(
				'SELECT * FROM contactus WHERE contactus_id = ? ', array($contactusId)
			);

			$returner = null;
			if ($result->RecordCount() != 0) {
				$returner = &$this->_returnContactusFromRow($result->GetRowAssoc(false));
			}
			$result->Close();
			unset($result);

			// Cache this Contactus.
			$this->contactusCache[$contactusId] =& $returner;

			return $returner;
	
		}
	
		function _returnContactUsFromRow(&$row){
			$contactus = &new Contactus();	
			
			$contactus->setContactusId($row['contactus_id']);
			$contactus->setEmail($row['email']);
			$contactus->setSubject($row['subject']);
			$contactus->setCaptcha($row['captcha']);
			$contactus->setMessage($row['message']);
		
			HookRegistry::call('Contactus::_returnContactusFromRow', array(&$contactus, &$row));
			return $contactus;
		}
	
		function &getContactUss(){
			$result = &$this->retrieveRange(
				'SELECT * FROM contactus'
			);
		
			$returner =& new DAOResultFactory($result, $this, '_returnContactusFromRow');
			return $returner;
		}
		
		/**
		* Insert a new ContactUs.
		* @param $contactus Contactus
		*/	
	 	function insertContactus(&$contactus) {
			$this->update(
				'INSERT INTO contactus
					( 
						email, 
						subject, 
						captcha, 
						message)
					VALUES
					(?,?,?,?)',
				array(
					 
					 $contactus->getEmail(),
					 $contactus->getSubject(),
					 $contactus->getCaptcha(),
					 $contactus->getMessage())
			);

			$contactusId = $this->getInsertContactusId();
			$contactus->setContactusId($contactusId);

			// Cache this response.
			$this->contactusCache[$contactus->getContactusId()] =& $returner;

			return $contactus->getContactusId();
		}
		
	 	function updateContactus(&$contactus) {
			return $this->update(
				'UPDATE contactus SET 
						email = ?,
						subject = ?,
						captcha = ?,
						message = ? 
					WHERE contactus_id = ?',
				array(
					 $contactus->getEmail(),
					 $contactus->getSubject(),
					 $contactus->getCaptcha(),
					 $contactus->getMessage(),
					 $contactus->getContactusId())
				);
		}
		
		/**
	 	* Get the ID of the last inserted contactus
		 * @return int
		 */
		function getInsertContactusId() {
			return $this->getInsertId('contactus', 'contactus_id');
		}
}

?>