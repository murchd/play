<?php
/**
 * This is a simple class to handle sending of email using mail()
 * written by david murch
 * 9th August 2007
 *
 */
class Email {
	var $sendToAddress;
	var $thefromAddress;
	var $thefromName;
	var $subject;
	var $body;
	var $contentType;
	var $charset;
	
	
	function Email($sendToAddress=null,$fromAddress=null,$fromName=null) {
		if($sendToAddress==null) {
			$this->sendToAddress = Config::getVar('email','sendto');
		} else {
			$this->sendToAddress = $sendToAddress;
		}
		if($fromAddress==null) {
			$this->thefromAddress = Config::getVar('email','fromaddress');
		} else {
			$this->thefromAddress = $fromAddress;
		}
		if($fromName==null) {
			$this->thefromName = Config::getVar('email','fromname');
		} else {
			$this->thefromName = $fromName;
		}
		$this->contentType = "text/html";
		$this->charset = "iso-8859-1";
	}
	
	function send() {
		$theHeaders = $this->getHeaders();
		if(@mail($this->sendToAddress, $this->subject, $this->body, $theHeaders)) {
			return true;
		}else{
			return false;
		}
	
	}
	/**
	 * This generates headers
	 *
	 * @return unknown
	 */
	function getHeaders() {
		$theHeaders  = "From: \"".$this->thefromName."\" <".$this->thefromAddress.">\r\n"; 
		$theHeaders .= "X-Mailer: PHP/" . phpversion() . "\r\n"; 
		$theHeaders .= "MIME-Version: 1.0\r\n"; 
		$theHeaders .= "Content-type: ".$this->contentType."; charset=".$this->charset."\r\n"; 
		return $theHeaders;
	}
	
	function setCharset($param) {
		$this->charset = $param;
	}
	
	function setContentType($param) {
		$this->contentType = $param;
	}
	
	
	
	function setRecipientAddress($param) {
		$this->sendToAddress = $param;
	}
	function setSenderName($param) {
		$this->thefromName = $param;
	}
	function setSenderAddress($param) {
		$this->thefromAddress = $param;
	}
	function setSubject($param) {
		$this->subject = $param;
	}
	function setBody($param) {
		$this->body = $param;
	}
	
}



















?>