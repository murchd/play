<?php

/**
 * @file DAOResultFactory.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package db
 * @class DAOResultFactory
 *
 * Wrapper around ADORecordSet providing "factory" features for generating 
 * objects from DAOs.
 *
 * $Id: LDAPResultFactory.inc.php,v 1.1 2009/11/16 20:34:40 dwm77 Exp $
 */

import('core.ItemIterator');

class LDAPResultFactory extends ItemIterator {
	/** The DAO used to create objects */
	var $dao;

	/** The name of the DAO's factory function (to be called with an associative array of values) */
	var $functionName;

	/** The LDAPRecordSet to be wrapped around */
	var $records;

	/** True iff the resultset was always empty */
	var $wasEmpty;

	var $isFirst;
	var $isLast;
	var $page;
	var $count;
	var $pageCount;

	/**
	 * Constructor.
	 * Initialize the DAOResultFactory
	 * @param $records object ADO record set
	 * @param $dao object DAO class for factory
	 * @param $functionName The function to call on $dao to create an object
	 */
	function LDAPResultFactory(&$records, &$dao, $functionName) {
		$this->functionName = $functionName;
		$this->dao = &$dao;

		if (!$records || $records['count'] == 0 ) {
			if ($records) unset($records);
			$this->records = null;
			$this->wasEmpty = true;
			$this->page = 1;
			$this->isFirst = true;
			$this->isLast = true;
			$this->count = 0;
			$this->pageCount = 1;
			$this->currentIndex = 0;
		}
		else {
			$this->records = &$records;
			$this->wasEmpty = false;
			$this->currentIndex = 0;
			$this->count = $records['count'] -1;
			
		}
	}

	/**
	 * Return the object representing the next row.
	 * @return object
	 */
	function &next() {
		$nullVar = null;
		if ($this->records == null) return $nullVar;
		if ($this->currentIndex <= $this->count) {

			$functionName = &$this->functionName;			
			$dao = &$this->dao;			
			$result = &$dao->$functionName($this->records[$this->currentIndex]);			
			$this->currentIndex++;			

			return $result;
			
		} else {
			$this->_cleanup();
			return $nullVar;
		}
	}

	/** Return the next row, with key.
	 * @return array ($key, $value)
	 */
	function &nextWithKey() {
		// We don't have keys with rows. (Row numbers might become
		// valuable at some point.)
		return array(null, $this->next());
	}

	function atFirstPage() {
		return $this->isFirst;
	}

	function atLastPage() {
		return $this->isLast;
	}

	function getPage() {
		return $this->page;
	}

	function getCount() {
		return $this->count;
	}

	function getPageCount() {
		return $this->pageCount;
	}

	/**
	 * Return a boolean indicating whether or not we've reached the end of results
	 * @return boolean
	 */
	function eof() {
		if ($this->records == null) return true;
		if ($this->currentIndex > $this->count) {
			$this->_cleanup();
			return true;
		}
		return false;
	}

	/**
	 * Return a boolean indicating whether or not this resultset was empty from the beginning
	 * @return boolean
	 */
	function wasEmpty() {
		return $this->wasEmpty;
	}

	/**
	 * PRIVATE function used internally to clean up the record set.
	 * This is called aggressively because it can free resources.
	 */
	function _cleanup() {
		unset($this->records);
		$this->records = null;
	}

	function &toArray() {
		$returner = array();
		while (!$this->eof()) {
			$returner[] = $this->next();
		}
		return $returner;
	}
}

?>
