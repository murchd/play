<?php

/**
 * @file DBConnection.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package db
 * @class DBConnection
 *
 * Class for accessing the low-level database connection.
 * Currently integrated with ADOdb (from http://php.weblogs.com/adodb/).
 *
 * $Id: LDAPConnection.inc.php,v 1.1 2009/11/16 20:34:40 dwm77 Exp $
 */

class LDAPConnection {
	
	/** The underlying database connection object */
	var $ldapconn;
	
	/** LDAP connection parameters */
	var $host;
	var $port;
	var $dsn;
	var $password;


	/** @var boolean establish connection on initiation */
	var $connectOnInit;
	
	/* @var boolean enable debugging output */
	var $debug;
	
	
	/** @var boolean indicate connection status */
	var $connected;
	
	/**
	 * Constructor.
	 * Calls initDefaultDBConnection if no arguments are passed,
	 * otherwise calls initCustomDBConnection with custom connection parameters. 
	 */
	function LDAPConnection() {
		$this->connected = false;
		$this->initDefaultLDAPConnection();
		
	}
	
	/**
	 * Create new database connection with the connection parameters from the system configuration.
	 * @return boolean
	 */
	function initDefaultLDAPConnection() {

		$this->host = Config::getVar('ldap', 'host');
		$this->port = Config::getVar('ldap', 'port');
		$this->dsn = Config::getVar('ldap', 'dsn');
		$this->password = Config::getVar('ldap', 'password');
		$this->debug = Config::getVar('database', 'debug') ? true : false;
		$this->connectOnInit = true;
		$this->forceNew = false;
		
		return $this->initConn();
	}
	
	
	/**
	 * Initialize database connection object and establish connection to the ldap server.
	 * @return boolean
	 */
	function initConn() {

		$this->ldapconn = ldap_connect($this->host,$this->port);
	
		if ($this->connectOnInit) {
			return $this->bind();
		} else {
			return true;
		}
	}
	
	function bind(){
		ldap_bind($this->ldapconn, $this->dsn, $this->password);		
	}
	
	function login($dsn = null , $password = null){
		if((isset($dsn)) && (isset($password))){
			if(@ldap_bind(self::getConn(), $dsn, $password)){
				return true;					
			}else{
				return false;			
			}
		}else{			
			return false;
		}
	}
	
	/**
	 * Disconnect from the tree.
	 */
	function disconnect() {
		if ($this->connected) {
			ldap_close($this->ldapconn);
			$this->connected = false;
		}
	}
	
	/**
	 * Reconnect to the tree.
	 * @param $forceNew boolean force a new connection
	 */
	function reconnect($forceNew = false) {
		$this->disconnect();
		if ($forceNew) {
			$this->persistent = false;
		}
		$this->forceNew = $forceNew;
		return $this->connect();
	}
	
	/**
	 * Return the database connection object.
	 * @return ADONewConnection
	 */
	function &getLDAPConn() {
		return $this->ldapconn;
	}
	
	/**
	 * Check if a database connection has been established.
	 * @return boolean
	 */
	function isConnected() {
		return $this->connected;
	}
	
	/**
	 * Return a reference to a single static instance of the database connection manager.
	 * @param $setInstance LDAPConnection
	 * @return DBConnection
	 */
	function &getInstance($setInstance = null) {
		static $instance;
		
		if (isset($setInstance)) {
			$instance = $setInstance;
		} else if (!isset($instance)) {
			$instance = new LDAPConnection();
		}
		
		return $instance;
	}
	
	/**
	 * Return a reference to a single static instance of the database connection.
	 * @return ADONewConnection
	 */
	function &getConn() {
		$conn = &LDAPConnection::getInstance();
		return $conn->getLDAPConn();
	}
}

?>
