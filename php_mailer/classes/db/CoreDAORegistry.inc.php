<?php
class CoreDAORegistry {

	/**
	 * Get the current list of registered DAOs.
	 * This returns a reference to the static hash used to
	 * store all DAOs currently instantiated by the system.
	 * @return array
	 */
	function &getDAOs() {
		static $daos = array();
		return $daos;
	}

	/**
	 * Register a new DAO with the system.
	 * @param $name string The name of the DAO to register
	 * @param $dao object A reference to the DAO to be registered
	 * @return object A reference to previously-registered DAO of the same
	 *    name, if one was already registered; null otherwise
	 */
	function &registerDAO($name, &$dao) {
		if (isset($daos[$name])) {
			$returner = &$daos[$name];
		} else {
			$returner = null;
		}
		$daos = &CoreDAORegistry::getDAOs();
		$daos[$name] = &$dao;
		return $returner;
	}

	/**
	 * Retrieve a reference to the specified DAO.
	 * @param $name string the class name of the requested DAO
	 * @param $dbconn ADONewConnection optional
	 * @return DAO
	 */
	function &getDAO($name, $dbconn = null) {
		$daos = &CoreDAORegistry::getDAOs();

		if (!isset($daos[$name])) {
			// Import the required DAO class.
			//echo '<br>core';
			//echo CoreDAORegistry::getQualifiedDAOName($name);
			//exit();
			
			import(CoreDAORegistry::getQualifiedDAOName($name));

			// Only instantiate each class of DAO a single time
			$daos[$name] = &new $name();

			if ($dbconn != null) {
				// FIXME Needed by installer but shouldn't access member variable directly
				$daos[$name]->_dataSource = $dbconn;
			}
		}

		return $daos[$name];
	}

	/**
	 * Return the fully-qualified (e.g. page.name.ClassNameDAO) name of the
	 * given DAO.
	 * @param $name string
	 * @return string
	 */
	function getQualifiedDAOName($name) {
		// FIXME This function should be removed (require fully-qualified name to be passed to getDAO?)

			switch ($name) {
			
				case 'SiteSettingsDAO': return 'site.SiteSettingsDAO';
				case 'PluginSettingsDAO': return 'plugins.PluginSettingsDAO';
				case 'SessionDAO': return 'session.SessionDAO';		
				case 'UserDAO': return 'user.UserDAO';
				case 'GroupDAO': return 'group.GroupDAO';
				case 'SessionDAO': return 'session.SessionDAO';
				case 'PageDAO': return 'page.PageDAO';
				case 'PageCategoryDAO': return 'pagecategory.PageCategoryDAO';
				case 'ProductCategoryDAO': return 'productcategory.ProductCategoryDAO';
				case 'PageModuleDAO': return 'pagemodule.PageModuleDAO';
				case 'ProductDAO': return 'product.ProductDAO';
				case 'CartDAO': return 'cart.CartDAO';
				case 'ImageDAO': return 'image.ImageDAO';
				case 'PageTemplateDAO': return 'pagetemplate.PageTemplateDAO';
				case 'GalleryDAO': return 'gallery.GalleryDAO';
				case 'ShopDAO': return 'shop.ShopDAO';
				case 'ModuleConfigDAO': return 'module.ModuleConfigDAO';
				case 'SiteSettingsDAO': return 'site.SiteSettingsDAO';
				case 'VersionDAO': return 'site.VersionDAO';
				case 'CronJobDAO': return 'cron.CronJobDAO';
				case 'EmailEventDAO': return 'emailmessage.eventmanager.EmailEventDAO';
				case 'EmailMessageDAO': return 'emailmessage.emailmailer.EmailMessageDAO';
				case 'FileDAO': return 'file.FileDAO';
				case 'CaptchaDAO': return 'captcha.CaptchaDAO';
				case 'EmailcampaignDAO': return 'emailcampaign.EmailcampaignDAO';
				case 'AutogenDAO': return 'install.AutogenDAO';
				case 'FreightdestinationDAO': return 'freightdestination.FreightdestinationDAO';
				case 'FreightDestinationLineDAO': return 'freightdestinationline.FreightDestinationLineDAO';
				case 'CalendarDAO': return 'calendar.CalendarDAO';
			
				default: fatalError('Unrecognized DAO ' . $name);
			}			
		
		return null;
	}
}
?>
