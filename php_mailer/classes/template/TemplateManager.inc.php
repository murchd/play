<?php

/**
 * @file TemplateManager.inc.php
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @package template
 * @class TemplateManager
 *
 * Class for accessing the underlying template engine.
 * Currently integrated with Smarty (from http://smarty.php.net/).
 *
 * $Id: TemplateManager.inc.php,v 1.2 2009/11/18 11:18:44 dwm77 Exp $
 */

/* This definition is required by Smarty */
define('SMARTY_DIR', Core::getBaseDir() . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'smarty' . DIRECTORY_SEPARATOR);

require_once('smarty/Smarty.class.php');


class TemplateManager extends Smarty {

	/**
	 * Constructor.
	 * Initialize template engine and assign basic template variables.
	 */
	function TemplateManager() {
		parent::Smarty();
		
		import('cache.CacheManager');

		// Set up Smarty configuration
		$baseDir = Core::getBaseDir();
		$cachePath = CacheManager::getFileCachePath();
		$this->template_dir = $baseDir . DIRECTORY_SEPARATOR . 'templates';
		$this->compile_dir = $cachePath . DIRECTORY_SEPARATOR . 't_compile';
		$this->config_dir = $cachePath . DIRECTORY_SEPARATOR . 't_config';
		$this->cache_dir = $cachePath . DIRECTORY_SEPARATOR . 't_cache';
		
		// TODO: Investigate caching behaviour and if Harvester2 can take advantage of it
		//$this->caching = true;
		//$this->compile_check = true;
		
		// Assign common variables
		$this->assign('defaultCharset', Config::getVar('i18n', 'client_charset'));
		$this->assign('baseUrl', Request::getBaseUrl());
		$this->assign('indexUrl', Request::getIndexUrl());
		$this->assign('pageUrl', Request::getPageUrl());
		$this->assign('currentPage', Request::getRequestedPage());
		$this->assign('currentNode', Request::getRequestedNode());
		$this->assign('pagePath', '/' . Request::getRequestedPage() . (($requestedOp = Request::getRequestedOp()) == '' ? '' : '/' . $requestedOp));
		$this->assign('currentUrl', Request::getCompleteUrl());
		$this->assign('dateFormatTrunc', Config::getVar('general', 'date_format_trunc'));
		$this->assign('dateFormatShort', Config::getVar('general', 'date_format_short'));
		$this->assign('dateFormatLong', Config::getVar('general', 'date_format_long'));
		$this->assign('datetimeFormatShort', Config::getVar('general', 'datetime_format_short'));
		$this->assign('datetimeFormatLong', Config::getVar('general', 'datetime_format_long'));
		$this->assign('currentLocale', Locale::getLocale());
		$this->assign('timestamp',time()); //UNIX Time Stamp
		$this->assign('publicFilesDir', Request::getBaseUrl() . '/public');
		
		if (!defined('SESSION_DISABLE_INIT')) {
			/* Kludge to make sure no code that tries to connect to the database is executed
			 * (e.g., when loading installer pages). */
			$sessionManager = &SessionManager::getManager();
			$session = &$sessionManager->getUserSession();
			$isUserLoggedIn = Validation::isLoggedIn();
			$this->assign_by_ref('userSession', $session);
			$this->assign('isUserLoggedIn', $isUserLoggedIn);			
			$this->assign('loggedInGroups', $session->getSessionVar('groups'));
			$this->assign('loggedInUsername', $session->getSessionVar('userName'));
			if($isUserLoggedIn){
				$userDao =& CoreDAORegistry::getDAO('UserDAO');
				$user =& $userDao->getUser($session->getSessionVar('userName'));
				$this->assign_by_ref('userLoggedIn', $user);
			}
			$site = &Request::getSite();

			$this->assign('siteTitle', $site->getTitle());

			$locales = &$site->getSupportedLocaleNames();

		} else {
			$locales = &Locale::getAllLocales();
			$this->assign('languageToggleNoUser', true);
		}

		if (isset($locales) && count($locales) > 1) {
			$this->assign('enableLanguageToggle', true);
			$this->assign('languageToggleLocales', $locales);
		}
		
		// Register custom functions
		$this->register_modifier('strip_unsafe_html', array('String', 'stripUnsafeHtml'));
		$this->register_modifier('get_value', array(&$this, 'smartyGetValue'));
		$this->register_modifier('to_array', array(&$this, 'smartyToArray'));
		$this->register_modifier('explode', array(&$this, 'smartyExplode'));
		$this->register_modifier('assign', array(&$this, 'smartyAssign'));
		$this->register_function('translate', array(&$this, 'smartyTranslate'));
		$this->register_function('flush', array(&$this, 'smartyFlush'));
		$this->register_function('call_hook', array(&$this, 'smartyCallHook'));
		$this->register_function('html_options_translate', array(&$this, 'smartyHtmlOptionsTranslate'));
		$this->register_block('iterate', array(&$this, 'smartyIterate'));
		$this->register_function('page_links', array(&$this, 'smartyPageLinks'));
		$this->register_function('page_info', array(&$this, 'smartyPageInfo'));
		$this->register_function('get_debug_info', array(&$this, 'smartyGetDebugInfo'));
		$this->register_function('url', array(&$this, 'smartyUrl'));
		$this->register_function('widget', array(&$this, 'smartyWidget'));
		$this->register_function('embedpage', array(&$this, 'smartyEmbedPage'));
		
		//$this->register_function('get_help_id', array(&$this, 'smartyGetHelpId'));
		//$this->register_function('icon', array(&$this, 'smartyIcon'));
		//$this->register_function('help_topic', array(&$this, 'smartyHelpTopic'));
		//$this->register_function('tab', array(&$this, 'smartyTab'));
		//$this->register_function('roundedfooter', array(&$this, 'smartyRoundedFooter'));
		//$this->register_function('menu', array(&$this, 'smartyMenu'));
		//$this->register_function('security_check', array(&$this, 'smartySecurityCheck'));
	}

	/**
	 * Dislay the template.
	 */
	function display($template, $sendContentType = 'text/html') {
		$charset = Config::getVar('i18n', 'client_charset');

		// Give any hooks registered against the TemplateManager
		// the opportunity to modify behavior; otherwise, display
		// the template as usual.
		if (!HookRegistry::call('TemplateManager::display', array(&$this, &$template, &$sendContentType, &$charset))) {
			// Explicitly set the character encoding
			// Required in case server is using Apache's AddDefaultCharset directive
			// (which can prevent browser auto-detection of the proper character set)
			if ($sendContentType !== null) {
				header('Content-Type: ' . $sendContentType . '; charset=' . $charset);
			}

			// Actually display the template.
			if($this->template_exists($template)){
				parent::display($template);
			}else{
				$templateMgr =& TemplateManager::getManager();
				$templateMgr->assign_by_ref('template',$template);
				parent::display('common/notFound.tpl');
				
			}
		}
	}

	/**
	 * Clear template compile and cache directories.
	 */
	function clearTemplateCache() {
		$this->clear_compiled_tpl();
		$this->clear_all_cache();
	}
	
	/**
	 * Return an instance of the template manager.
	 * @return TemplateManager the template manager object
	 */
	function &getManager() {
		static $instance;
		
		if (!isset($instance)) {
			$instance = new TemplateManager();
		}
		return $instance;
	}
	
	
	//
	// Custom template functions, modifiers, etc.
	//
	
	function smartyWidget($params, &$smarty) {
		
		$widget =& WidgetRegistry::getWidget($params['name']);

		return $widget->displayWidget($params);
		
	}
	
	
	/**
	 * Smarty usage: {translate key="localization.key.name" [paramName="paramValue" ...]}
	 *
	 * Custom Smarty function for translating localization keys.
	 * Substitution works by replacing tokens like "{$foo}" with the value of the parameter named "foo" (if supplied).
	 * @params $params array associative array, must contain "key" parameter for string to translate plus zero or more named parameters for substitution.
	 * 	Translation variables can be specified also as an optional
	 * 	associative array named "params".
	 * @params $smarty Smarty
	 * @return string the localized string, including any parameter substitutions
	 */
	function smartyTranslate($params, &$smarty) {
		if (isset($params) && !empty($params)) {
			if (isset($params['key'])) {
				$key = $params['key'];
				unset($params['key']);
				if (isset($params['params'])) {
					$paramsArray = $params['params'];
					unset($params['params']);
					$params = array_merge($params, $paramsArray);
				}
				return Locale::translate($key, $params);
				
			} else {
				return Locale::translate('');
			}
		}
	}
	
	/**
	 * Smarty usage: {translate var="varName" key="localization.key.name" [paramName="paramValue" ...]} 
	 *
	 * Same as Smarty translate except translated string is assigned to variable.
	 * @see TemplateManager#smartyTranslate
	 */
	function smartyAssignTranslate($params, &$smarty) {
		if (isset($params['var'])) {
			$smarty->assign($params['var'], $smarty->smartyTranslate($params, $smarty));
		}
	}
	
	/**
	 * Smarty usage: {security_check function="functionName" action="view||create||delete" [params="1,2 etc" ...]} 
	 *
	 * Centralises Security handleing based on Users assigned roles,  removes the need for the template 
	 * to know about roles and Privileges
	 */
	function smartySecurityCheck($params, &$smarty) {
		$class =& $params['class'];
		$action =& $params['action'];
		
		
		
		if (isset($params['var'])) {
			$varName = $params['var'];
			// NOTE: CANNOT use $this because it belongs to
			// another object in certain versions of PHP!
			$templateMgr =& TemplateManager::getManager();
			if(isset($params['object'])){
				$object =& $params['object'];
				$templateMgr->assign($varName, Validation::checkPrivileges($class,$action,$object));
			}else{
				$templateMgr->assign($varName, Validation::checkPrivileges($class,$action));				
			}
		}
		
	}
	
	
	/**
	 * Smarty usage: {html_options_translate ...}
	 * For parameter usage, see http://smarty.php.net/manual/en/language.function.html.options.php
	 *
	 * Identical to Smarty's "html_options" function except option values are translated from i18n keys.
	 * @params $params array 
	 * @params $smarty Smarty
	 */
	function smartyHtmlOptionsTranslate($params, &$smarty) {
		if (isset($params['options'])) {
			if (isset($params['translateValues'])) {
				// Translate values AND output
				$newOptions = array();
				foreach ($params['options'] as $k => $v) {
					$newOptions[Locale::translate($k)] = Locale::translate($v);
				}
				$params['options'] = $newOptions;
				
			} else {
				// Just translate output
				$params['options'] = array_map(array('Locale', 'translate'), $params['options']);
			}
			
		}
		
		if (isset($params['output'])) {
			$params['output'] = array_map(array('Locale', 'translate'), $params['output']);
			
		}
		
		if (isset($params['values']) && isset($params['translateValues'])) {
			$params['values'] = array_map(array('Locale', 'translate'), $params['values']);
		}
		
		require_once($smarty->_get_plugin_filepath('function','html_options'));
		return smarty_function_html_options($params, $smarty);
	}

	/**
	 * Iterator function for looping through objects extending the
	 * ItemIterator class.
	 * Parameters:
	 *  - from: Name of template variable containing iterator
	 *  - item: Name of template variable to receive each item
	 *  - key: (optional) Name of variable to receive index of current item
	 */
	function smartyIterate($params, $content, &$smarty, &$repeat) {
		$iterator = &$smarty->get_template_vars($params['from']);

		if (isset($params['key'])) {
			if (empty($content)) $smarty->assign($params['key'], 1);
			else $smarty->assign($params['key'], $smarty->get_template_vars($params['key'])+1);
		}

		if ($iterator && !$iterator->eof()) {
			$repeat = true;

			if (isset($params['key'])) {
				list($key, $value) = $iterator->nextWithKey();
				$smarty->assign_by_ref($params['item'], $value);
				$smarty->assign_by_ref($params['key'], $key);
			} else {
				$smarty->assign_by_ref($params['item'], $iterator->next());
			}
		} else {
			$repeat = false;
		}
		return $content;
	}

	/**
	 * Smarty usage: {get_help_id key="(dir)*.page.topic" url="boolean"}
	 *
	 * Custom Smarty function for retrieving help topic ids.
	 * Direct mapping of page topic key to a numerical value representing the associated help topic xml file
	 * @params $params array associative array, must contain "key" parameter for string to translate
	 * @params $smarty Smarty
	 * @return numerical help topic id
	 */
	function smartyGetHelpId($params, &$smarty) {
		if (isset($params) && !empty($params)) {
			if (isset($params['key'])) {
				$key = $params['key'];
				unset($params['key']);
				$translatedKey = Help::translate($key);
			} else {
				$translatedKey = Help::translate('');
			}
			
			if ($params['url'] == "true") {
				return Request::url('help', 'view', explode('/', $translatedKey));
			} else {
				return $translatedKey;
			}
		}
	}

	/**
	 * Smarty usage: {help_topic key="(dir)*.page.topic" text="foo"}
	 *
	 * Custom Smarty function for creating anchor tags
	 * @params $params array associative array
	 * @params $smarty Smarty
	 * @return anchor link to related help topic
	 */
	function smartyHelpTopic($params, &$smarty) {
		if (isset($params) && !empty($params)) {
			$translatedKey = isset($params['key']) ? Help::translate($params['key']) : Help::translate('');
			$link = Request::url('help', 'view', explode('/', $translatedKey));
			$text = isset($params['text']) ? $params['text'] : '';
			return "<a href=\"$link\">$text</a>";
		}
	}

	/**
	 * Smarty usage: {icon name="image name" alt="alternative name" url="url path"}
	 *
	 * Custom Smarty function for generating anchor tag with optional url
	 * @params $params array associative array, must contain "name" paramater to create image anchor tag
	 * @return string <a href="url"><img src="path to image/image name" ... /></a>
	 */
	function smartyIcon($params, &$smarty) {
		if (isset($params) && !empty($params)) {
			$iconHtml = '';
			if (isset($params['name'])) {
				// build image tag with standarized size of 16x16
				$disabled = (isset($params['disabled']) && !empty($params['disabled']));
				$iconHtml = '<img src="' . $smarty->get_template_vars('baseUrl') . '/templates/images/icons/';			
				$iconHtml .= $params['name'] . ($disabled ? '_disabled' : '') . '.gif" width="16" height="14" border="0" alt="';
				
				// if alt parameter specified use it, otherwise use localization version
				if (isset($params['alt'])) {
					$iconHtml .= $params['alt'];
				} else {
					$iconHtml .= Locale::translate('icon.'.$params['name'].'.alt');
				}
				$iconHtml .= '" ';

				// if onclick parameter specified use it
				if (isset($params['onclick'])) {
					$iconHtml .= 'onclick="' . $params['onclick'] . '" ';
				}


				$iconHtml .= '/>';

				// build anchor with url if specified as a parameter
				if (!$disabled && isset($params['url'])) {
					$iconHtml = '<a href="' . $params['url'] . '" class="icon">' . $iconHtml . '</a>';
				}
			}
			return $iconHtml;
		}
	}

	/**
	 * Display page information for a listing of items that has been
	 * divided onto multiple pages.
	 * Usage:
	 * {page_info from=$myIterator}
	 */
	function smartyPageInfo($params, &$smarty) {
		$iterator =& $params['iterator'];
		$itemsPerPage = $params['items'];
		
		if (!is_numeric($itemsPerPage)) {
			$itemsPerPage = Config::getVar('interface', 'items_per_page');
		}

		$page = $iterator->getPage();
		$pageCount = $iterator->getPageCount();
		$itemTotal = $iterator->getCount();

		if ($pageCount<1) return '';

		return Locale::translate('navigation.items', array(
			'from' => (($page - 1) * $itemsPerPage) + 1,
			'to' => min($itemTotal, $page * $itemsPerPage),
			'total' => $itemTotal
		));
	}

	/**
	 * Flush the output buffer. This is useful in cases where Smarty templates
	 * are calling functions that take a while to execute so that they can display
	 * a progress indicator or a message stating that the operation may take a while.
	 */
	function smartyFlush($params, &$smarty) {
		flush();
		ob_flush();
	}

	/**
	 * Call hooks from a template.
	 */
	function smartyCallHook($params, &$smarty) {
		$output = null;
		HookRegistry::call($params['name'], array(&$params, &$smarty, &$output));
		return $output;
	}

	/**
	 * Get debugging information and assign it to the template.
	 */
	function smartyGetDebugInfo($params, &$smarty) {
		if (Config::getVar('debug', 'show_stats')) {
			$smarty->assign('enableDebugStats', true);
			$smarty->assign('debugExecutionTime', Core::microtime() - Registry::get('system.debug.startTime'));
			$dbconn = &DBConnection::getInstance();
			$smarty->assign('debugNumDatabaseQueries', $dbconn->getNumQueries());
			$smarty->assign_by_ref('debugNotes', Registry::get('system.debug.notes'));
		}

	}
	function smartyRoundedFooter($params, &$smarty){
		$output .= 		'<div class="SectionFooter">
							<div class="wrap1">
						 		<div class="wrap2">
						 			<div class="wrap3">
						 				
						 			</div>
						 		</div>
						 	</div>
						</div>';
		
		return $output;
	}
	
	function smartyTab($params, &$smarty){
		$paramList = array('page', 'op', 'path', 'anchor', 'escape', 'onClick');

		foreach ($paramList as $param) {
			if (isset($params[$param])) {
				$$param = $params[$param];
				unset($params[$param]);
			} else {
				$$param = null;
			}
		}
		$output = '';
		if($page != ''){
		$output .= '<a href="'.Request::url($page, $op, $path).'" style="text-decoration:none;">';
		}
		
		$output .= 		'<div dojoType="FisheyeListItem" caption="'.$params['title'].'" class="SectionHeader" ';
							if($params['selected']){
									$output .= 'id="CurrentTab" ';
			
							}else{
			
									$output .= 'id="Tab" ';
							}
							if($onClick){			
									$output .= 'onClick="'.$onClick.'" ';
							}
							
							
			$output .= '><div class="wrap1">
						 		<div class="wrap2">
						 			<div class="wrap3">
						 				<strong style="margin-left:0px;">'.$params['title'].'</strong>
						 			</div>
						 		</div>
						 	</div>
						</div>';
		if($page != ''){
		$output .= '</a>';
		}			

		return $output;
		
	}
	/**
	 * Generate a URL into OJS.
	 */
	function smartyUrl($params, &$smarty) {
		// Extract the variables named in $paramList, and remove them
		// from the params array. Variables remaining in params will be
		// passed along to Request::url as extra parameters.
		$paramList = array('page', 'op', 'path', 'anchor', 'escape');

		foreach ($paramList as $param) {
			if (isset($params[$param])) {
				$$param = $params[$param];
				unset($params[$param]);
			} else {
				$$param = null;
			}
		}

		return Request::url($page, $op, $path, $params, $anchor, !isset($escape) || $escape);
	}

	/**
	 * Display page links for a listing of items that has been
	 * divided onto multiple pages.
	 * Usage:
	 * {page_links
	 * 	name="nameMustMatchGetRangeInfoCall"
	 * 	iterator=$myIterator
	 * }
	 */
	function smartyPageLinks($params, &$smarty) {
		$iterator =& $params['iterator'];
		$name = $params['name'];
		unset($params['iterator']);
		unset($params['name']);

		$numPageLinks = $smarty->get_template_vars('numPageLinks');
		if (!is_numeric($numPageLinks)) $numPageLinks=10;

		$page = $iterator->getPage();
		$pageCount = $iterator->getPageCount();
		$itemTotal = $iterator->getCount();

		$pageBase = max($page - floor($numPageLinks / 2), 1);
		$paramName = $name . 'Page';

		if ($pageCount<=1) return '';

		$value = '';

		if ($page>1) {
			$params[$paramName] = 1;
			$value .= '<a class="pagelinks" href="' . Request::url(null, null, Request::getRequestedArgs(), $params) . '"> First </a>&nbsp;';
			$params[$paramName] = $page - 1;
			$value .= '<a class="pagelinks" href="' . Request::url(null, null, Request::getRequestedArgs(), $params) . '"> Previous </a>&nbsp;';
		}else{
			$value .= 'First &nbsp; Previous';
		}

		for ($i=$pageBase; $i<min($pageBase+$numPageLinks, $pageCount+1); $i++) {
			if ($i == $page) {
				$value .= ",$i,&nbsp;";
			} else {
				$params[$paramName] = $i;
				$value .= '<a class="pagelinks" href="' . Request::url(null, null, Request::getRequestedArgs(), $params) . '">' . $i . '</a>&nbsp;';
			}
		}
		if ($page < $pageCount) {
			$params[$paramName] = $page + 1;
			$value .= '<a class="pagelinks" href="' . Request::url(null, null, Request::getRequestedArgs(), $params) . '"> Next </a>&nbsp;';
			$params[$paramName] = $pageCount;
			$value .= '<a class="pagelinks" href="' . Request::url(null, null, Request::getRequestedArgs(), $params) . '"> Last </a>&nbsp;';
		}else{
			$value .= 'Next &nbsp; Last';
		}

		return $value;
	}

	/**
	 * PRIVATE function, used by assignPaging to generate a URL with the
	 * specified page number. Replaces the current page number if necessary.
	 */
	function _makePageUrl($currentUrl, $paramName, $pageNum) {
		$url = parse_url($currentUrl);
		if (empty($url['query'])) return "$currentUrl?$paramName=$pageNum";
		if (substr_count($url['query'], "$paramName=")>0) {
			$array=explode("$paramName=", $url['query']);
			$array2=explode('&',$array[1]);
			$url['query']=str_replace("$paramName=$array2[0]", "$paramName=$pageNum", $url["query"]);
			return $this->_glueUrl($url);
		}
		else return "$currentUrl&$paramName=$pageNum";
	}

	function _glueUrl($url) {
		if (!is_array($url)) return false;
	
		$returner = @$url['scheme'] ? @$url['scheme'] . ':' . ((strtolower(@$url['scheme']) == 'mailto') ? '' : '//') : '';
		$returner .= @$url['user'] ? @$url['user'] . (@$url['pass']? ':' . @$url['pass']:'') . '@' : '';
		$returner .= @$url['host'] ? @$url['host'] : '';
		$returner .= @$url['port'] ? ':' . @$url['port'] : '';
		$returner .= @$url['path'] ? @$url['path'] : '';
		$returner .= @$url['query'] ? '?' . @$url['query'] : '';
		$returner .= @$url['fragment'] ? '#'. @$url['fragment'] : '';
		return $returner;
	}

	/**
	 * Convert the parameters of a function to an array.
	 */
	function smartyToArray() {
		return func_get_args();
	}

	/**
	 * Get the value of a template variable.
	 */
	function smartyGetValue($name) {
		$templateMgr =& TemplateManager::getManager();
		return $templateMgr->get_template_vars($name);
	}

	/**
	 * Split the supplied string by the supplied separator.
	 */
	function smartyExplode($string, $separator) {
		return explode($separator, $string);
	}

	/**
	 * Assign a value to a template variable.
	 */
	function smartyAssign($value, $varName, $passThru = false) {
		if (isset($varName)) {
			// NOTE: CANNOT use $this because it belongs to
			// another object in certain versions of PHP!
			$templateMgr =& TemplateManager::getManager();
			$templateMgr->assign($varName, $value);
		}
		if ($passThru) return $value;
	}
	
	function smartyEmbedPage($params, &$smarty) {
				
		$handler =& PageHandlerRegistry::getPageHandler($params['page']);
		$op = $params['op'];
		
		return $handler->$op($params);
	}
}

?>
