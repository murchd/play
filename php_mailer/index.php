<?php

/**
 * Handle a new request.
 */
function handleRequest() {
	
	

	
	// Determine the handler for this request
	$page = Request::getRequestedPage();
	$op = Request::getRequestedOp();
	$sourceFile = sprintf('pages/%s/index.php', $page);

	// If a hook has been registered to handle this page, give it the
	// opportunity to load required resources and set HANDLER_CLASS.
	if (!HookRegistry::call('LoadHandler', array(&$page, &$op, &$sourceFile))) {
		if (file_exists($sourceFile)) require($sourceFile);
		else require('pages/index/index.php');
	}

	define('SESSION_DISABLE_INIT',1);
	session_start();

	$methods = array_map('strtolower', get_class_methods(HANDLER_CLASS));

	if (in_array(strtolower($op), $methods)) {
		// Call a specific operation
		call_user_func(array(HANDLER_CLASS, $op), Request::getRequestedArgs());
		
	} else {
		// Call the selected handler's index operation
		call_user_func(array(HANDLER_CLASS, 'index'), Request::getRequestedArgs());
	}
}

// Initialize system and handle the current request
require('includes/driver.inc.php');
initSystem();
handleRequest();

?>