<?php

// remember to replace you@email.com with your own email address lower in this code.

// load the variables form address bar
$subject = $_POST["subject"];
$message = $_POST["message"];
$from = $_POST["from"];
$verif_box = $_POST["verif_box"];

// remove the backslashes that normally appears when entering " or '
$message = stripslashes($message);
$subject = stripslashes($subject);
$from = stripslashes($from);

// check to see if verificaton code was correct
if(md5($verif_box).'a4xn' == $_COOKIE['tntcon']){
	// if verification code was correct send the message and show this page
	mail("rhys@rhyschapman.com", 'rhyschapman.com contact form: '.$subject, "".$message, "From: $from");
	// delete the cookie so it cannot sent again by refreshing this page
	setcookie('tntcon','');
} else if(isset($message) and $message!=""){
	// if verification code was incorrect then return to contact page and show error
	header("Location:".$_SERVER['HTTP_REFERER']."?subject=$subject&from=$from&message=$message&wrong_code=true");
	exit;
} else {
	echo "no variables received, this page cannot be accessed directly";
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>E-Mail Sent</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

body {
	background-color: #333;
}

.thankstext {
	font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
}

.thankstextcolor {
	color: #FFF;
}

.thankstext .thankstextcolor {
	font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
}

.thankstext .thankstextcolor {
	font-size: 18px;
}

a:link {
	color: #0F0;
}

a:visited {
	color: #999;
}

a:hover {
	color: #0F0;
}

a:active {
	color: #0F0;
}
-->
</style>
</head>

<body class="thankstext">
<span class="thankstextcolor">Email sent. Thank you.<br />
<br />
Return to <a href="/">home page</a> ? </span>
</body>
</html>
