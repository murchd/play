var ValidateForm = {
warningColor: '#ffe6e6',
backgroundColor: '#ffffff',
backgroundImage: '',
backgroundWorning: '',

//submitmessage: 'There are errors with you request<br />Please correct the highlighted fields and resubmit. <div title=\'close\' class=\'closeWarning\' onClick=\'ValidateForm.closeWarning(this)\'>x</div>', 
submitmessage: 'There are errors with your request \n Please correct the highlighted fields and resubmit.', 

createvalidationArrays: function(){
	/*
	Validation types
	*/
	
	ValidateForm.validationSet = new Array(); 
	ValidateForm.validationSet['email'] = /^.+?@.+?\..+$/;
	ValidateForm.validationSet['number'] = /^[0-9]+$/;
	ValidateForm.validationSet['currency'] = /^\s*[$]?\s*((\d+)|(\d{1,3}(\,\d{3})+))(\.\d{2})?\s*$/;
	ValidateForm.validationSet['dob'] = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/(\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/(\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/(\d{2}))|(29\/02\/((0[48]|[2468][048]|[13579][26])|(00))))$/;
	ValidateForm.validationSet['name'] = /^[a-zA-Z]+$/;
	ValidateForm.validationSet['fullName']= /^[a-zA-Z]+(([\ \'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$/;
	ValidateForm.validationSet['phone'] = /^[- ()0-9]+$/;
	
	/*
	Warning messages
	*/
	ValidateForm.warningMessage = new Array();
	ValidateForm.warningMessage['email'] = "This input requires that you have a format like someone@somewhere.com";
	ValidateForm.warningMessage['number'] = "This input requires that you have a format like 123456";
	ValidateForm.warningMessage['currency'] = "This input requires that you have a format like $500,000";
	ValidateForm.warningMessage['dob'] = "This input requires that you have a format like dd/mm/yy";
	ValidateForm.warningMessage['name'] = "This input requires that you have a format like Joe Bloggs";
	ValidateForm.warningMessage['phone'] = "This input requires that you have a format like (03) 399 8999";
	ValidateForm.warningMessage['fullName'] = "This input requires that you have a format like Joe Bloggs";
		
	ValidateForm.validationFunctions = new Array();
	ValidateForm.validationFunctions['text'] = ValidateForm.check_text;
	ValidateForm.validationFunctions['radio'] = ValidateForm.check_radio;
	ValidateForm.validationFunctions['SELECT'] = ValidateForm.check_select;
	ValidateForm.validationFunctions['checkbox'] = ValidateForm.check_check;
	ValidateForm.validationFunctions['TEXTAREA'] = ValidateForm.check_text;
	ValidateForm.validationFunctions['password'] = ValidateForm.check_text;
	
},
check_check: function(theElement){
		if(theElement.checked){
			theElement.style.backgroundColor = ValidateForm.backgroundColor;
			theElement.parentNode.style.backgroundColor = ValidateForm.backgroundColor;
			return true;
		}else{
			ValidateForm.valid = false;
			theElement.style.backgroundColor = ValidateForm.warningColor;
			theElement.parentNode.style.backgroundColor = ValidateForm.warningColor;
			return false;
		}
},
check_radio: function(theElement){
thisRadioVaild = false;
thisList = ValidateForm.form[theElement.name];
		for(r=0;r<thisList.length;r++){
			thisRadio = thisList[r];
			if(thisRadio.checked){
				thisRadioVaild =true;
			}
		}
		if(thisRadioVaild ==true){
			theElement.style.backgroundColor = ValidateForm.backgroundColor;
			theElement.parentNode.style.backgroundColor = ValidateForm.backgroundColor;
			return true;
		}else{
			ValidateForm.valid = false;
			theElement.style.backgroundColor = ValidateForm.warningColor;
			theElement.parentNode.style.backgroundColor = ValidateForm.warningColor;
			return false;
		}
},
check_text: function(theElement){
	if(theElement.value != ''){
		theElement.style.backgroundImage= ValidateForm.backgroundImage;
		theElement.style.backgroundColor = ValidateForm.backgroundColor;
		return true;		
	}else{
		theElement.style.backgroundImage=ValidateForm.backgroundWorning;
		theElement.style.backgroundColor = ValidateForm.warningColor;
		ValidateForm.valid = false;
		return false;
	}	
},
check_select: function(theElement){
	if(theElement.value != ''){
		theElement.style.backgroundColor = ValidateForm.backgroundColor;
		return true;		
	}else{
		theElement.style.backgroundColor = ValidateForm.warningColor;
		ValidateForm.valid = false;
		return false;
	}	
	
},
init: function(){
	this.addEvent(this.form,'submit',this.validateMe,false);
	this.createvalidationArrays();
	for (i=0;i < this.form.elements.length;i++){
		theElement = this.form.elements[i];
		if(theElement.attributes.getNamedItem('format')){
			this.addEvent(theElement,'blur',this.checkMe,false);
		}
		
		if(theElement.type == 'submit'){
				this.addEvent(theElement,'click',this.validateMe,false);
		}
		
		
	}	
},
createWarningElement: function(){
	ValidateForm.warningDiv = document.createElement("div");
	ValidateForm.warningDiv.id = 'warningDiv';
	ValidateForm.warningDiv.style.backgroundImage = ValidateForm.backgroundWorning;
	ValidateForm.warningDiv.style.backgroundColor = ValidateForm.warningColor;
	ValidateForm.warningDiv.className = 'warningDiv_format';	
},
addEvent: function (elm,evType,fn,useCapture){
	if (elm.addEventListener){
		elm.addEventListener(evType,fn,useCapture);
		return true;
	}else if(elm.attachEvent){
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}else{
		elm['on' + evType] = fn;
	}
},
closeWarning: function(theDiv){
	ValidateForm.warningDiv = false;
	theDiv.parentNode.style.display = 'none';
},

validateMe: function(e){
	if(ValidateForm.warningDiv){
		ValidateForm.warningDiv= false;
	}
	ValidateForm.valid = true;

	for (i=0;i < ValidateForm.form.elements.length;i++){
		theElement = ValidateForm.form.elements[i];
		if(theElement.attributes.getNamedItem('required')){
			if(theElement.nodeName == 'INPUT'){
			ValidateForm.validationFunctions[theElement.type](theElement);
			}else{
			ValidateForm.validationFunctions[theElement.nodeName](theElement);
			}
		}
		if(theElement.attributes.getNamedItem('format')){
			ValidateForm.checkMeValidationTime(theElement);
		}
	}
	
	if(window.event){
		if(ValidateForm.valid){
			window.event.returnValue = true;					
		}else{
			alert(ValidateForm.submitmessage);
			/*if(!ValidateForm.warningDiv){
				ValidateForm.createWarningElement();
				window.document.body.appendChild(ValidateForm.warningDiv);
			}
			ValidateForm.warningDiv.style.display='none';
			ValidateForm.warningDiv.style.display='block';
			ValidateForm.warningDiv.className = 'warningDiv_submit';
			
			ValidateForm.warningDiv.style.top = (window.document.body.scrollTop+30) + 'px';
			ValidateForm.warningDiv.style.left = (window.document.body.clientWidth/2) -100 +'px';
			ValidateForm.warningDiv.innerHTML = ValidateForm.submitmessage;*/
			window.event.returnValue = false;	
		}
	}else{
		if(!ValidateForm.valid){
			/*if(!ValidateForm.warningDiv){
				ValidateForm.createWarningElement();
				window.document.body.appendChild(ValidateForm.warningDiv);
			}*/
			alert(ValidateForm.submitmessage);
			/*ValidateForm.warningDiv.style.display='block';
			ValidateForm.warningDiv.className = 'warningDiv_submit';
			ValidateForm.warningDiv.id = "submissionError";
			ValidateForm.warningDiv.style.top = (window.pageYOffset+30) + 'px';
			ValidateForm.warningDiv.style.left = (window.innerWidth/2) -100 +'px';
			ValidateForm.warningDiv.innerHTML = ValidateForm.submitmessage;*/
			e.preventDefault();
		}
	}
},
checkMeValidationTime: function (theElement){
		
	var validateType = theElement.attributes.getNamedItem('format').value;
	var re = ValidateForm.validationSet[validateType];
	if((!theElement.value.match(re)) && (theElement.value != '')){
	theElement.style.backgroundColor = ValidateForm.backgroundColor;
		theElement.style.backgroundColor = ValidateForm.warningColor;
		ValidateForm.valid = false;
		return false;
	}else{
		if(theElement.value != ''){
		theElement.style.backgroundImage=ValidateForm.backgroundWorning;
		
		theElement.style.backgroundImage= ValidateForm.backgroundImage;
		}
		return true;	
	}

},
checkMe: function (e){
	if(window.event){
		theActiveElement = window.event.srcElement;		
	}else{
		theActiveElement =  e.target;
	}
	var validateType = theActiveElement.attributes.getNamedItem('format').value;
	var re = ValidateForm.validationSet[validateType];	
	var message = ValidateForm.warningMessage[validateType];
	if(!(theActiveElement.value.match(re)) && (theActiveElement.value != '')){
			if(!ValidateForm.warningDiv){
				ValidateForm.createWarningElement();
			}
		ValidateForm.warningDiv.innerHTML =  message + '<div title=\'close\' class=\'closeWarning\' onClick=\'ValidateForm.closeWarning(this)\'>x</div>';
		ValidateForm.warningDiv.style.display='block';
		if(!window.event){
			ValidateForm.warningDiv.style.width = (theActiveElement.offsetWidth -6) + 'px';
		
		}else{
			
			ValidateForm.warningDiv.style.width = theActiveElement.offsetWidth + 'px';
		}
		theActiveElement.style.backgroundColor = ValidateForm.warningColor;
		//theActiveElement.parentNode.style.position = 'relative';
		theActiveElement.parentNode.appendChild(ValidateForm.warningDiv);
	
	}else{
	theActiveElement.style.backgroundColor = ValidateForm.backgroundColor;
		if (window.document.getElementById('warningDiv')){
			var warningDiv = window.document.getElementById('warningDiv');
			if (warningDiv.parentNode == theActiveElement.parentNode){
				theActiveElement.parentNode.removeChild(warningDiv);
			}
		}
	}

}
};