/**
 * general.js
 *
 * Copyright (c) 2005-2006 Alec Smecher and John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Site-wide common JavaScript functions. 
 *
 * $Id: general.js,v 1.1 2009/11/16 20:34:40 dwm77 Exp $
 */

/**
 * Prompt user for confirmation prior to loading a URL.
 */
function confirmAction(url, msg) {
	if (confirm(msg)) {
		if (url) {
			document.location.href=url;
		}
		return true;
	}
	return false;
}

/**
 * Open window displaying help.
 */
function openHelp(url) {
	window.open(url, 'Help', 'width=700,height=600,screenX=100,screenY=100,toolbar=0,scrollbars=1');
}

/**
 * Open window displaying comments.
 */
function openComments(url) {
	window.open(url, 'Comments', 'width=700,height=600,screenX=100,screenY=100,toolbar=0,resizable=1,scrollbars=1');
}

/**
 * Open window for preview.
 */
function openWindow(url) {
	window.open(url, 'Window', 'width=600,height=550,screenX=100,screenY=100,toolbar=0,resizable=1,scrollbars=1');
}

/**
 * Open window for reading tools.
 */
function openRTWindow(url) {
	window.open(url, 'RT', 'width=700,height=500,screenX=100,screenY=100,toolbar=0,resizable=1,scrollbars=1');
}
function openRTWindowWithToolbar(url) {
	window.open(url, 'RT', 'width=700,height=500,screenX=100,screenY=100,toolbar=1,resizable=1,scrollbars=1');
}

/**
 * browser object availability detection
 * @param objectId string of object needed
 * @param style int (0 or 1) if style object is needed
 * @return javascript object specific to current browser
 */
function getBrowserObject(objectId, style) {
	var isNE4 = 0;
	var currObject;

	// browser object for ie5+ and ns6+
	if (document.getElementById) {
		currObject = document.getElementById(objectId);
	// browser object for ie4+
	} else if (document.all) {
		currObject = document.all[objectId];
	// browser object for ne4
	} else if (document.layers) {
		currObject = document.layers[objectId];
		isNE4 = 1;
	} else {
		// do nothing
	}
	
	// check if style is needed
	if (style && !isNE4) {
		currObject = currObject.style;
	}
	
	return currObject;
}

/**
 * Load a URL.
 */
function loadUrl(url) {
	document.location.href=url;	
}


function toggleBackground(theElement,className){
   	if(theElement.className == className){
   		theElement.className = '';   		
   	}else{
   		theElement.className = className;
   		
   	}
}

function menuItemOver(menuItem){

}
function menuItemOut(menuItem){

}

function highlightDiagram(selectedHTML){
	
	/*
	diagramNode = window.document.getElementById('diagram');
		
	diagramHolder = diagramNode.parentNode;
	diagramHolder.removeChild(diagramNode);
	if(window.event){
		
		targetParam = diagramNode.childNodes[1];
	
	
	}else{
		targetParam = diagramNode.childNodes[3];
	}
	currentContent = targetParam.value;
	targetParam.value = currentContent + '&selectedHTML=' + selectedHTML;
	targetParam.data = "";
	
	diagramHolder.appendChild(diagramNode);*/
}

function unHighlightDiagram(){
	
	/*
	diagramNode = window.document.getElementById('diagram');
		
	diagramHolder = diagramNode.parentNode;
	if(window.event){
		targetParam = diagramNode.childNodes[1];
	}else{
		targetParam = diagramNode.childNodes[3];
	}
	currentContent = targetParam.value;
	stringParts = currentContent.split('&');	
	targetParam.value = stringParts[0];
	
	
	diagramHolder.removeChild(diagramNode);
	diagramHolder.appendChild(diagramNode);*/
}

function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}


/*
Rico Ajax Handling
*/

function buildAjaxMessage(messageBody){
		var xmlRequestTemplate = '<ajax-request><request>' + messageBody + '</request></ajax-request>';
		return xmlRequestTemplate
	}


var Utf8 = {

	// public method for url encoding
	encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// public method for url decoding
	decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}

function openProject(theElement){

	if (theElement.checked == true){
	
		if(window.event){
			$('projectArea').style.display = 'block';
		}else{
			$('projectArea').style.display = 'table-row';
			
		}
		
		if ($('projectMonetaryRange').value == 'specified' ){
			$('projectMonetaryRangeFrom').required = 'true';
			$('projectMonetaryRangeTo').required = 'true';
		}
		
		
		$('projectRationalObjective').required = 'true';
	}else{
		$('projectArea').style.display = 'none';
		$('projectRationalObjective').removeAttribute('required');
		$('projectMonetaryRangeFrom').removeAttribute('required');
		$('projectMonetaryRangeTo').removeAttribute('required');
	}
}

function checkMonetaryRange(theElement){

	if(theElement.value== 'specified'){
		if(window.event){
			$('projectSpecifedRangeArea').style.display = 'block';			
		}else{
			$('projectSpecifedRangeArea').style.display = 'table-row';			
		}
		$('projectMonetaryRangeFrom').required = 'true';
		$('projectMonetaryRangeTo').required = 'true';
	}else{
		$('projectSpecifedRangeArea').style.display = 'none';
		$('projectMonetaryRangeFrom').removeAttribute('required');
		$('projectMonetaryRangeTo').removeAttribute('required');
	}

}