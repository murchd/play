<?php

class IndexHandler extends Handler {
	
	function index($args = array()) {
		import('contactus.ContactusHandler');
		ContactusHandler::index($args);
	}
}
?>