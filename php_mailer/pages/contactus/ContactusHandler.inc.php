<?php
class ContactusHandler extends Handler {
	function index($args = array()) {
		ContactusHandler::editContactus($args);
	}
	function editContactus($args = array()) {
		$contactusId = $args[0];
	
		import("contactus.form.ContactusForm");
	
		$contactusForm = new ContactusForm($contactusId);
		$contactusForm->initData();
		$contactusForm->display();
	}
	
	function updateContactus() {
		$contactusId = Request::getUserVar("contactusId");
	
		import("contactus.form.ContactusForm");
	
		$contactusForm = new ContactusForm($contactusId);
		$contactusForm->initData();
		$contactusForm->readInputData();
		if($contactusForm->validate()) {
			$contactusForm->execute();
			Request::redirect('contactus','success');
		}else{
			$contactusForm->display();
		}
	}
	
	function success() {
		$templateMgr =& TemplateManager::getManager();
		$templateMgr->display('contactus/message.tpl');
	}
	
}

		
?>