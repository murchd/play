use glennaz_db;
drop table header;
create table header(
	parent int not null,
        poster char(20) not null,
        title char(20) not null,
        children int default 0 not null,
        area int default 1 not null,
        posted datetime not null,
        postid int unsigned not null auto_increment primary key
);
drop table body;
create table body
(
        postid int unsigned not null primary key,
        message text
);

INSERT INTO header (parent,poster,title,children,posted) values("0","David","Testing","0","2006-01-26 10:50:44.");

INSERT INTO header (parent,poster,title,children,posted) values("0","David","Testing again","0","2006-01-26 10:52:44.");
