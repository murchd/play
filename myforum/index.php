<?
session_start();

include ('include_fns.php');


//check if we have created our session variable
if(!session_is_registered('expanded'))
{
	$expanded = array();
	session_register('expanded');
}

// check is an expand button was pressed
// expand might equal 'all' or a postid or not be set
if($expand == 'all')
{
	expand_all($expanded);
}
else
{
	$expanded[$expand] = true;
}

// check if a collapse button was pressed
// collapse might equal all or a postif or not be set
if($collapse)
{
	if($collapse == 'all')
		unset($expanded);
	else
		unset($expanded[$collapse]);
}

do_html_header("Discussion Posts");

display_index_toolbar();

// display the tree view of conversations
display_tree($expanded);

do_html_footer();
?>
