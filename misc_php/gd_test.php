<?php


								/*****************************************************************************
								// Image resize. Requires GD 2.0.1 or later (2.0.28 or later recommended)
								/*****************************************************************************/
								
								// Image resize source image
								$image_prefix = 'thumb_';
							  	$source = 'test_large.jpg';
								$dest = $image_prefix.$source;
								
								// Get new dimensions
								list($width, $height, $itype, $iattr) = getimagesize($source);
								$thumb_size = 150;
								
								if($height > $width)
								{
								$factor = $height / $thumb_size;
								$new_height = $thumb_size;
								$new_width = $width / $factor;
								}
								
								if($width > $height)
								{
								$factor = $width / $thumb_size;
								$new_width = $thumb_size;
								$new_height = $height / $factor;
								}
								// Resample
								$new_im = imagecreatetruecolor($new_width,$new_height);
								$im = imagecreatefromjpeg($source);
								imagecopyresampled($new_im,$im,0,0,0,0,$new_width,$new_height,$width,$height);
								// Output
								imagejpeg($new_im,$dest,75);
								

?>
<table border>
<tr>
<td>
<a href="test_large.jpg"><img src="thumb_test_large.jpg" border="0"></a>
</td>
</tr>
<tr>
<td></td>
</tr>
</table>

