<?php
//my round function
function myround($mrNumber) {

	//obtain floor value
	$thenumberFloor = floor($mrNumber);
	echo ("<br>The number floor ".$thenumberFloor."<br>");
	
	//obtain the decimal value
	$decimalValue = ($mrNumber - $thenumberFloor);
	echo ("The decimal value ".$decimalValue."<br>");
	
	//printf function
	print("<br>Printf returns the correct rounded amount with correct significant figures but is printed and cant be stored in a variable<br>");
	printf("%.2f",$mrNumber);
	$testval = sprintf("%.2f",$mrNumber);
	print("<br><br>");
	
	return $testval;
}


//check if form was submitted
if (array_key_exists('number_field', $_POST)) {
	// pull form value from post
	$theNumber = $_POST["number_field"];
	
	//display the raw number submitted in the form 
	echo("You entered ".$theNumber."<br>");
	
	//The different round function
	$builtin = round($theNumber, 2);
	//$myown = round($theNumber + 0.0001 / pow(10, 2), 2);
	$myown = myround($theNumber);
	
	//Display the rounded numbers
	echo("The number rounded to 2 decimal places ".$builtin." (Using PHP built-in round() function) NOT WHAT I WANT<br> " .
		 "The number rounded to 2 decimal places ".$myown." (Using my own PHP function, sprintf) YAY!!<br><br> " .
		 "<a href=".$PHP_SELF.">Back to form</a>");
} else {
	//If the form has not been submitted display the form
	echo("<form name=form1 action=".$PHP_SELF." method=post> " .
		"<input type=decimal size=40 name=number_field> " .
		"<input type=submit value=Submit> " .
		"</form> ");
}
?>
