<% @Language=JScript EnableSessionState=false %>
<%
Response.Expires = -1;

var addr = String(Request.ServerVariables("HTTP_X_FORWARDED_FOR"));

if (addr == "undefined")
{
   	addr = String(Request.ServerVariables("HTTP_CLIENT_IP"));

    if (addr == "undefined")
   	{
       	addr = Request.ServerVariables("REMOTE_ADDR");
    }
}
%>
<% = addr %>