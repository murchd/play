<?php
require_once('includes/driver.inc.php');

$page = Request::getRequestedPage();
$op = Request::getRequestedOp();
if (!$page){
	$page = 'news';	
}

$pageHandler =& PageHandler::getPageHandler();
$pageHandler->setPage($page);
$pageHandler->setTransformation('pageTransform.xsl');

$pageHandler->display();
?>