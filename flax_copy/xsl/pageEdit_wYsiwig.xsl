<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="common.xsl"/>

<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MODA FOTOGRAFICA - Christchurch Photographers - <xsl:value-of select="pageData/title" /></title>


<script type="text/javascript" >

window.onload = function(){

generate_wysiwyg('textarea1');

	
}



</script>


<style type="text/css">
.h2{
	font-family:Helvetica Neue;
	background-color:#CCFFFF;
	border:1px dotted #000000;
	padding:10px;
	margin-left:-15px;
	font-size:1.2em;
}
.paragraph{
	font-family:Helvetica Neue;
	background-color:#CCFFFF;
	border:1px dotted #000000;
	padding:10px;
	margin-left:-15px;
	height:auto;
	overflow:auto;
	
}
body {
	width:100%;
	height:100%;
	text-align: center;
	/*background: #339999 url('images/filigre_medium.png');*/
	background: #000000 url('<xsl:value-of select="$baseUrl" />/images/filigre_medium_darkgrey.png');
	margin:0px;
	font-family:Helvetica Neue;
	color:#323232;
	line-height:1.5em;
}

h1, h2, h3, h4{
	font-family:Helvetica Neue;
	fornt-size:0.9em;
	font-weight:lighter ;

}
p {
	font-size:0.9em;
}

#container{
	position:relative;
	left:0px;
	margin:0pt auto;
	min-height:100%;
	padding:0px;
	top:0px;
	text-align:left;
	width:775px;
}
#header{
	width:743px;
	height:313px;
	background: #FFFFFF url('<xsl:value-of select="$baseUrl" />/images/m_footer_small.png') center no-repeat;
	margin: 0 auto;
	
}

.roundedDiv{
	background:url('<xsl:value-of select="$baseUrl" />/images/corners/tl.png') no-repeat scroll top left;
	border:1px solid #000000;
	padding:-1px;
}




#leftColumn {
	float:left;
	margin-left: 25px;
	width:401px;

}
#rightColumn {
	margin-left: 35px;
	float:left;
	width:248px;
}
#wrap1 {
	padding-top:20px;
	background:url('<xsl:value-of select="$baseUrl" />/images/allcorners_wide.png') center top no-repeat;

}

#wrap2{
	
	margin-bottom:-20px;
	background:url('<xsl:value-of select="$baseUrl" />/images/allcorners_wide.png') center bottom no-repeat;
	padding:22px;
}


#colouredWrap1 {
	padding-top:20px;
	background: #000000  url('<xsl:value-of select="$baseUrl" />/images/allcorners.png') center top no-repeat;

}

#colouredWrap2{
	
	margin-bottom:20px;
	background:  #000000 url('<xsl:value-of select="$baseUrl" />/images/allcorners.png') center bottom no-repeat;

}
#wrap2 h3 ,input{

margin-top:-15px;
}

.leftMargin{
	position:absolute;
	left:0px;
	height:1000px;
	background: url('<xsl:value-of select="$baseUrl" />/images/left_margin.png') repeat-y;
	width:16px;
	
}
#contents{
	position:absolute;
	left:16px;
	height:1000px;
	background-color:#FFFFFF;
}
#tools{
	position:absolute;
	left:16px;
	bottom:50px;
	z-index:100;
	
	background-color:#FFFFFF;
}
.rightMargin{
	position:absolute;
	right:0px;
	height:1000px;
	background: url('<xsl:value-of select="$baseUrl" />/images/right_margin.png') repeat-y;
	width:16px;
}

#navigation{
	height:40px;
	padding-top:2px;
	
	text-align:center;
	
	margin-bottom:20px;
}



#navigation ul {
	padding-bottom:2px;
	list-style-type:none;


}
#navigation ul li {
	display:inline;
}

#navigation a {
	text-transform:uppercase;
	padding:10px 10px 1px 10px;
	display:inline;
	
	border-bottom:3px solid #FFFFFF;
	color:#323232;
	font-weight:lighter;
	text-decoration:none;
}

#navigation a:hover {
	/*background-color:#626262;*/
	border-bottom:3px solid #EEEEEE;
	color:#000000;
}

</style>



</head>
<body>
<form name="contactForm" method="post" action="/~kristianthornley/flax/flax/testpost.php" >
<!--  <form name="contentForm" method="post"  enctype="multipart/form-data" >
	<xsl:attribute name="action" ><xsl:value-of select="$baseUrl" />/index.php/<xsl:value-of select="$page" />/_update</xsl:attribute>	
	-->
	<div id="container">
	
			<div class="leftMargin">
		
			</div>
		
			<div id="contents">
			
				<div id="header">
					
				
				
				</div>
				<xsl:call-template name="navigation" />
				<div id="content">
					
					<div id="leftColumn">
						
						<div id="wrap1">
							<div id="wrap2">
								<input type="text" name="title" size="35" class="h2" >
									<xsl:attribute name="value" >
										<xsl:value-of select="pageData/title" />
									</xsl:attribute>
								</input>
								
								<textarea cols="42"  id="pageContent" >
									
								
									<xsl:copy-of select="." />
								</textarea>
											
								
							</div>
						</div>
						
						<select name="inputControl" id="inputControl" >
							<option value="P">Paragraph</option>
							<option value="UL">Unordered List</option>
						</select>
						
						<input type="button" onClick="appendInputElementToContent()" value="Add Element"/>
								
					</div>
					<div id="rightColumn">
						<div id="colouredWrap1">
							<div id="colouredWrap2" style="text-align:center;padding:20px;">
								<img >
									<xsl:attribute name="src">
										<xsl:value-of select="$baseUrl" />/images/<xsl:value-of select="pageData/image" />
									</xsl:attribute>
								</img>
								
								
							</div>
							
						</div>
						<input type="file" name="image" size="20" >
									<xsl:attribute name="value" >
										<xsl:value-of select="pageData/image" />
									</xsl:attribute>
							</input>
					</div>
					
					
				
				</div>
			<div id="tools">
				<input type="submit" value="Save Changes"  style="float:left;" />
				<input type="button" value="Cancel Changes"  style="float:right;" >
					<xsl:attribute name="onClick">window.location= '<xsl:value-of select="$baseUrl" />/index.php/<xsl:value-of select="$page" />'</xsl:attribute>
				</input>
			</div>
			
			</div>	
			
			<div class="rightMargin">
		
			</div>
	
		
	</div>
	</form>
</body>
</html>
</xsl:template>

</xsl:stylesheet>