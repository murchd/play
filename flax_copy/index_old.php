<?php
require_once('includes/driver.inc.php');

$page = Request::getRequestedPage();
$op = Request::getRequestedOp();
if (!$page){
	$page = 'news';	
}


$xmlFile = Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('data','directory') . DIRECTORY_SEPARATOR . $page .'.xml';
$domDocument = new DOMDocument();
$domDocument->load($xmlFile);

if($op == '_update'){
	import('file.FileManager');
	//import('page.PageHandler');
	
	$contentFragment = $domDocument->createDocumentFragment();
	$contentNode = $domDocument->getElementsByTagName('content');
	$imageNode = $domDocument->getElementsByTagName('image');
	$titleNode = $domDocument->getElementsByTagName('title');
	
	if(FileManager::uploadedFileExists('image')){
		$imageName = FileManager::getUploadedFileName('image'); 
		$imageNode->item(0)->nodeValue = $imageName;
		FileManager::uploadFile('image',Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('files','images') . DIRECTORY_SEPARATOR . $imageName);
	}
	
	
	
	// Specify configuration
	$config = array(
	           'indent'         => true,
	           'output-xhtml'   => true,
	           'show-body-only' =>true,
	           'wrap'           => 200);
	
	// Tidy
	$tidy = new tidy;
	$tidy->parseString(Request::getUserVar('content'), $config, 'utf8');
	$tidy->cleanRepair();
	
	
	
	
	//echo $titleNode->nodeValue;
	//$imageNode->nodeValue = Request::getUserVar('image');
	$titleNode->item(0)->nodeValue = Request::getUserVar('title');
	
	
	$contentFragment->appendXML('<content>'.$tidy. '</content>');
	$contentNode->item(0)->parentNode->replaceChild($contentFragment,$contentNode->item(0));
	$domDocument->save($xmlFile);
}



if($op == "index"){
	$xslFile = Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('transformation','directory') . DIRECTORY_SEPARATOR . Config::getVar('transformation','stylesheet') ;
}else{
	if($op == '_edit'){
		$xslFile = Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('transformation','directory') . DIRECTORY_SEPARATOR . Config::getVar('transformation','stylesheet_edit') ;	
	}else{
		$xslFile = Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('transformation','directory') . DIRECTORY_SEPARATOR . Config::getVar('transformation','stylesheet') ;			
	}
}

$processorFile = new DOMDocument();
$processorFile->load($xslFile);


$xslProcessor = new XSLTProcessor();
$xslProcessor->setParameter('','baseUrl',Request::getBaseUrl());
$xslProcessor->setParameter('','page',$page);
$xslProcessor->importStylesheet($processorFile);

echo $xslProcessor->transformToXml($domDocument);
?>