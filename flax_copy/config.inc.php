; <?php exit(); // DO NOT DELETE ?>

[general]

disable_path_info = Off


[data]

directory = 'data'
cache = 'cache'

[transformation]

directory = 'xsl'
stylesheet = 'pageTransform.xsl'
stylesheet_edit = 'pageEdit.xsl'


[files]

images = 'images'