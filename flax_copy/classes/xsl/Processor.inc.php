<?php

import('xml.XMLHandler');
class Processor {
	
	
	var $_processor;
	var $_params = array();
	
	
	function Processor(){
		
		if(class_exists('XSLTProcessor')){
			$this->_processor = new XSLTProcessor();
		}else if(function_exists('xsl_create')){
			$this->_processor = xsl_create();
		}else if(function_exists('domxml_xslt_stylesheet_file')){
			//Its ok we can create when we have the sheet			
		}else{
			return false;
		}
		
	}
	
	function setParameter($key,$value){
		$this->_params[$key] = $value; 
	}
	
	function load($xslFilePath){
		
		$this->_xslFilePath = $xslFilePath;
		$this->_xslDom = new XMLHandler($xslFilePath);
		
		if(class_exists('XSLTProcessor')){
			$this->_processor->importStylesheet($this->_xslDom->getDom());
		}else if(function_exists('xsl_create')){
			 
			//Its ok we we will do this on the process 
		}else if(function_exists('domxml_xslt_stylesheet_file')){
			$this->_processor = domxml_xslt_stylesheet_file($this->_xslFilePath);	
		}else{
			return false;
		}
		
	}
	
	function process($xmlFilePath){
		
		$this->_xmlFilePath = $xmlFilePath;
		$this->_xmlDom = new XMLHandler($xmlFilePath);
		
		
		if(class_exists('XSLTProcessor')){
			
		
			foreach ($this->_params as $key => $value){
				$this->_processor->setParameter('',$key ,$value);		
			}
			
			$xml = $this->_processor->transformToXML($this->_xmlDom->getDom());
			
			
			return $xml;
		}else if(function_exists('xsl_create')){
			$xml = xsl_process($this->_processor, $this->_xslFilePath, $this->_xslFilePath);
			return $xml;
			//Its ok we we will do this on the process 
		}else if(function_exists('domxml_xslt_stylesheet_file')){
			$doc = $this->_processor->process($this->_xmlDom->getDom());	
			return $doc->dump_mem(true);
		}else{
			return false;
		}
		
	}
	
	
}

?>