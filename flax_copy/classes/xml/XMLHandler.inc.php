<?php
class XMLHandler{
	
	var $_domDoc;
	
	
	function XMLHandler($pathToXML = null){
		if($pathToXML){
			if(class_exists('DOMDocument')){
				$this->_domDoc = new DOMDocument();
				$this->_domDoc->load($pathToXML);
			}else if(function_exists('domxml_new_doc')){
				$this->_domDoc = domxml_open_file($pathToXML);
			}else{
				fatalError('No XML Processing Library');
			}
		}else{
			if(class_exists('DOMDocument')){
				$this->_domDoc = new DOMDocument('1.0', 'utf-8');			
			}else if(function_exists('domxml_new_doc')){
				$this->_domDoc = domxml_new_doc();
			}else{
				fatalError('No XML Processing Library');
			}
			
		}
		
	}

	
	function loadHTML($xml){
		if(class_exists('DOMDocument')){
			$this->_domDoc->loadHTML($xml);				
		}else if(function_exists('domxml_new_doc')){
			$this->_domDoc = domxml_open_mem($xml);
		}else{
			fatalError('No XML Processing Library');
		}	
		
	}
	
	function save($filePath){
		if(class_exists('DOMDocument')){
			$this->_domDoc->save($filePath);				
		}else if(function_exists('domxml_new_doc')){
			$this->_domDoc->dump_file($filePath);
		}else{
			fatalError('No XML Processing Library');
		}
		
	}
	
	function toXHTML(){
		if(class_exists('DOMDocument')){
			return $this->_domDoc->saveHTML();				
		}else if(function_exists('domxml_new_doc')){
			$this->_domDoc->dump_mem(true);
		}else{
			fatalError('No XML Processing Library');
		}	
		
	}
	
	function getDOM(){
		
		return $this->_domDoc;
	}
	
}
?>