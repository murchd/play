<?php

class Handler {
	
var $xslProcessor;
	
	/*
	 * DOM Document Load for transformation
	 */
	var $_dataObject;
	
	/*
	 * DOM Document to transform with
	 */
	var $_transformationObject;
	
	/*
	 * XSLT processor
	 */
	var $_xslProcessor;
	
	/*
	 * Path to transformation
	 */
	var $_transformation;
	
	/*
	 * Path to data file
	 */
	var $_data;
	
	
	function Handler(){
		$this->_xslProcessor = new Processor();
		
	}
	
	
	function setData($page){
		$this->_data = $page;
		$this->_xslProcessor->setParameter('','data',$data);
	}
	
	function resolveDataPath(){
		return Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('data','directory') . DIRECTORY_SEPARATOR;
	}
	function resolveTransformationPath(){
		return Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('transformation','directory') . DIRECTORY_SEPARATOR;		
	}
	

	
	
	/**
	 * Force the Transformation
	 *
	 * @param String $transformationFileName
	 */
	function setTransformation($transformationFileName){
		$this->_transformation = $transformation;
	}
	
	function loadData(){
		$this->_pageData = new DomDocument();
		$xmlFilePath = $this->resolveDataPath() . $this->_page;
		$this->_pageData->load($xmlFilePath);		
	}
	function loadTransformation(){
		$this->_xslData = new DomDocument();
		$xmlFilePath = $this->resolveTransformationPath() . $this->_transformation;
		$this->_xslData->load($xmlFilePath);	
		
	}
}

?>