<?php
import('xsl.Processor');
import('xml.XMLHandler');

class PageHandler {
	
	var $_xmlDom;
	var $_xslDom;
	
	/**
	 * What transformation to carry out
	 *
	 * @var string
	 */

	var $_transformation;	
	
	/**
	 * What page is requested
	 *
	 * @var string
	 */

	var $_page;
	
	function PageHandler($page = null){
		$this->_xslProcessor = new Processor();
		$this->_xslProcessor->setParameter('baseUrl',Request::getBaseUrl());
		if($page != null){
			$this->setPage($page);
		}else{
			$this->setPage('index');
			
		}
	}
	
	function setPage($page){
		//if($page){
			$this->_page = $page;
		//}
		
		$this->_xslProcessor->setParameter('page',$page);
	}
	
	function resolveDataPath(){
		return Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('data','directory') . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR;
	}
	
	function resolveTransformationPath(){
		return Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('transformation','directory') . DIRECTORY_SEPARATOR;		
	}
	
	function resolveCachePath(){
		return Core::getBaseDir() . DIRECTORY_SEPARATOR . Config::getVar('data','cache') . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR;		
	}
	
	/**
	 * Force the Transformation
	 *
	 * @param String $transformationFileName
	 */

	function setTransformation($transformationFileName){
		$this->_transformation = $transformationFileName;
	}
	
	function loadData(){
		
		$xmlFilePath = $this->resolveDataPath() . $this->_page;
		$this->_xmlDom = new XMLHandler($xmlFilePath);		
	}
	function loadTransformation(){
		$xmlFilePath = $this->resolveTransformationPath() . $this->_transformation;
		$this->_xslDom = new XMLHandler($xmlFilePath);
		
		
	}
	
	function display(){
			
			
			
			if(file_exists($this->resolveCachePath() . $this->_page . '.html')){
				
				//header("Location: " . Request::getBaseUrl() . '/cache/pages/' .  $this->_page . '.html');
				
				$returner = new XMLHandler($this->resolveCachePath() . $this->_page . '.html');
				
			}else{

			
				$returner = new XMLHandler();
				
				$this->_xslProcessor->load($this->resolveTransformationPath() . $this->_transformation);
				$xhtml = $this->_xslProcessor->process($this->resolveDataPath() . $this->_page . '.xml');
				$returner->loadHTML($xhtml);
				$returner->save($this->resolveCachePath() . $this->_page . '.html');
				
				
			
			}
				
			echo $returner->toXHTML();
		
		
	}
	
	function getPageHandler(){
		static $instance;
		
		if(!isset($instance)){
			$instance = new PageHandler();
		}
		
		return $instance;
		
	}
	
	
}


?>