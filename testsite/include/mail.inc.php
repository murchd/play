<script language="javascript" type="text/javascript">
<!-- Hide from very old browsers than no one uses anymore 
// Check that a message has been entered
function checkForm(form1) {
if (form1.ccomment.value.length == 0) {
   form1.ccomment.value = alert("You forgot to enter your message!");
   form1.ccomment.value = '';
   form1.ccomment.focus();
   return false;
  }
// Check that message is not longer than 1000 characters
  if (form1.ccomment.value.length > 1000) {
   comment = (form1.ccomment.value);
   form1.ccomment.value = alert("Please enter a message less than 1000 charaters");
   form1.ccomment.value = comment;
   form1.ccomment.focus();
   return false;
  }
// Check that a name has been entered
  if (form1.cname.value.length == 0) {
   form1.cname.value = alert("You forgot to enter your name!");
   form1.cname.value = '';
   form1.cname.focus();
   return false;
  }
// Check that an from email address has been entered
  if (form1.cemail.value.length == 0) {
   form1.cemail.value = alert("You forgot to enter your Email address!");
   form1.cemail.value = '';
   form1.cemail.focus();
   return false;
  }
// If all the checks pass return true!!
  return true;
}
//-->
</script>

<?php
/**********************************************************************************
The following functions handle formatting and submiting the email message
Once the message is formatted correctly the Variables are sent to mail() which is
the mail function handled by php
***********************************************************************************/
//check that someone has submitted the form
if (array_key_exists('submitcontact', $_POST)) {

	//pull variables out of form array (form)
	$ccomment = $_POST['ccomment'];
	$cname = $_POST['cname'];
	$cemail = $_POST['cemail'];
	$cuseragent = $_POST['cuseragent'];
	$cremoteaddr = $_POST['cremoteaddr'];

	// Filter out HTML code as this could be malisious
	$ccomment = htmlspecialchars($ccomment);
	$cname = htmlspecialchars($cname);
	$cemail = htmlspecialchars($cemail);

	// apply formatting to message eg paragraphs and line breaks
	$ccomment = ereg_replace("\r","",$ccomment);
	$ccomment = ereg_replace("\n\n","<p>",$ccomment);
	$ccomment = ereg_replace("\n","<br>",$ccomment);
	$ccomment = eregi_replace("  ","&nbsp;&nbsp;",$ccomment);


	// Format mail headers and send form data as an email
	$fromname = 'EMNZ Enquiry';
	$fromemail = 'noreply@emnz.com';
	//$sendto = 'naturefarm@xtra.co.nz';
	$sendto = 'david@davidmurch.com';
	$subject = 'EMNZ Enquiry';
	// $msgbody is how you change the layout of the email as to where the data gets displayed
	$msgbody = ("<font size=2>This email has been send from an online form at www.emnz.com</font><br><br><b>Message </b><br>" . $ccomment . "<br><br> <b>Name:</b> " .$cname . "<br><b> Email: </b>" . $cemail . "<br>");
	$headers  = "From: \"".$fromname."\" <".$fromemail.">\r\n"; 
	$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n"; 
	$headers .= "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

	// Core mail funtion that actually sends the message passing the above variables into it
		$ok = mail($sendto, $subject, $msgbody, $headers);

	// Error handling
	if ($ok) {
		echo("<p>Thankyou. Your email as been sent.</p>");
	} else {
		echo("<p>An Error has occured your email has not been sent. </p>");
	}
} else {
	include 'include/mail_form.inc.php';
}
?>