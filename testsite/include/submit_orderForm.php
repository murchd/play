<?php $querystring = $_GET['pageid']; ?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?><?php echo ("?pageid=".$querystring.""); ?>" method="post" name="form1" onSubmit="return checkForm(this)">
<h2>Checkout Order</h2>
  <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
  <tr>
      <td colspan="2"><p><strong>Your Contact Information:</strong></p></td>
    </tr>
  <tr>
      <td valign="top"><p><strong>Name *</strong></p></td>
      <td><input name="oname" type="text" size="30" maxlength="30"></td>
    </tr>
    <tr>
      <td valign="top"><p><strong>E-mail *</strong></p></td>
      <td><input name="oemail" type="text" size="30" maxlength="60"></td>
    </tr> 
    <tr>
      <td width="130" valign="top"><p><strong>Address *</strong></p></td>
      <td><textarea name="oaddress" cols="30" rows="3" id="oaddress"></textarea></td>
    </tr>
    <tr>
      <td><p><strong>City *</strong></p></td>
	  <td><input name="ocity" type="text" size="30" maxlength="60"></td>
    </tr>
	 <tr>
      <td><p><strong>Country *</strong></p></td>
	  <td><input name="ocountry" type="text" size="30" maxlength="60"></td>
    </tr>
	<tr>
      <td><p><strong>Phone Number 1 *</strong></p></td>
	  <td><input name="ophone1" type="text" size="30" maxlength="60"></td>
    </tr>
	<tr><!-- Not required -->
      <td><p><strong>Phone Number 2</strong></p></td>
	  <td><input name="ophone2" type="text" size="30" maxlength="60"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
	  <td><p><font size="1">* Required</font></p></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td valign="top"><input type="submit" name="submit_order" value="Submit Order">
        
      </td>
    </tr>
	<tr>
	<td colspan="2"><p><font size="1">*WARNING* The information that you submit in this form is sent in plain text. It is, though unlikely, possible that someone could intercept this form data as it is transmitted over the internet.</font></p></td>
  </table>
</form>