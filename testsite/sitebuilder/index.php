<?
//Set no caching
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<html>
<head>
<title>davidmurch.com - Site Builder</title>

<link href="../stylesheet.css" rel="stylesheet" type="text/css" />
</head><body bgcolor="#999999">
<table border width="760" bgcolor="#ffffff">
  <tr>
    <td valign="top" width="200"><h2>Site Builder</h2>
      By David Murch
      <p><b>Pages</b></p>
      <a href="<?php echo("" . $PHP_SELF . "?search=yes"); ?>">Create / Edit
      / Delete a Page</a><br>
      <p><b>Meta/Line Area</b></p>
      <a href="<?php echo("" . $PHP_SELF . "?page=cmla"); ?>">Create a Meta/Line
      Area</a><br>
      <a href="<?php echo("" . $PHP_SELF . "?searchmla=yes"); ?>">Edit a Meta/Line
      Area</a><br>
      <p><b>Special Pages</b></p>
	  <a href="<?php echo("" . $PHP_SELF . "?page=addproduct"); ?>">Add a Product</a><br>
	  <a href="<?php echo("" . $PHP_SELF . "?searchproduct=yes"); ?>">Edit / Delete a Product</a><br>
	  
      <p><b>System</b></p>
      <a href="<?php echo("" . $PHP_SELF . "?page=viewsystem"); ?>">System Status</a><br>
      <a href="<?php echo("" . $PHP_SELF . "?page=exesql"); ?>">Execute SQL</a><br>
      <br>
      <a href="<?php echo("" . $PHP_SELF . "?page=install"); ?>">Install System</a><br>
    </td>
    <td valign="top"><?php
// if user wants to search a page
if (isset($search)):
include '../include/db.inc.php';
include 'search.php';
endif;
?>
      <?php
// if user wants to search a meta line area
if (isset($searchmla)):
include '../include/db.inc.php';
include 'searchmla.php';
endif;
?>
      <?php
// if user wants to create a page
if ($page == cp):
include 'createpage.php';
endif;
?>
      <?php
// if user wants to add a catagory
if ($page == cmla):
include 'createmetaline.php';
endif;
?>
      <?php
// if user wants to add a catagory
if ($page == emla):
include '../include/db.inc.php';
include 'editmetaline.php';
endif;
?>
      <?php
// if user wants to deletepage a page
if ($page == dp):
include '../include/db.inc.php';
include 'deletepage.php';
endif;
?>
      <?php
// if user wants to edit a page
if ($page == ep):
include '../include/db.inc.php';
include 'editpage.php';
else:
echo '&nbsp;';
endif;
?>
      <?php
// if user wants to view system
if ($page == viewsystem):
include 'viewsystem.php';
endif;
?>
      <?php
// if user wants to install
if ($page == install):
include '../include/db.inc.php';
include 'install.php';
endif;
?>
      <?php
// if user wants to exe sql
if ($page == exesql):
include '../include/db.inc.php';
include 'exesql.php';
endif;
?>
<?php
// if user wants to search a product
if (isset($searchproduct)):
include '../include/db.inc.php';
include 'special_searchproducts.php';
endif;
?>
<?php
// if user wants to add a product
if ($page == addproduct):
include '../include/db.inc.php';
include 'special_addproduct.php';
endif;
?>
      <?php
// if user wants to add a catagory
if ($page == editproduct):
include '../include/db.inc.php';
include 'special_editproduct.php';
endif;
?>
      <?php
// if user wants to add a catagory
if ($page == deleteproduct):
include '../include/db.inc.php';
include 'special_deleteproduct.php';
endif;
?>
    </td>
  </tr>
</table>
</body>
</html>
