#
#
# CREATE TABLES
#
#

drop table Customer;
create table Customer(CustID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			LastName CHAR(50),
			FirstName CHAR(50),
			Address CHAR(200),
			City CHAR(30),
			County CHAR(30),
			Phone1 CHAR(15),
			Phone2 CHAR(15),
			Email CHAR(100),
			CreditCardNum INT,
			CreditCardName CHAR(50),
			CreditCardExp CHAR(10),
			Balance DECIMAL(8,2));

drop table OrderLine;
create table OrderLine(OrderLineID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			OrderNum INT,
			ProductNum INT);

drop table Orders;
create table Orders(OrderNum INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			CustID INT,
			DateOrdered DATE,
			DateShipped DATE);

drop table Product;
create table Product(ProductNum INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			Name CHAR(50),
			Type CHAR(50),
			Description TEXT,
			Price DECIMAL(8,2),
			SOH INT,
			PhotoLink TEXT);

#
#
# INSERT SAMPLE RECORDS
#
#

INSERT INTO Product VALUES('null','Sample Product','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','10.20',19,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 2','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','199.99',4,'sample_product2.jpg');

INSERT INTO Product VALUES('null','Sample Product 3','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','34.99',12,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 4','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','5.40',5,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 5','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','19.17',16,'sample_product2.jpg');

INSERT INTO Product VALUES('null','Sample Product 6','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','27.14',7,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 7','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','24.95',32,'sample_product2.jpg');

INSERT INTO Product VALUES('null','Sample Product 8','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','99.99',44,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 9','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','100.00',14,'sample_product2.jpg');

INSERT INTO Product VALUES('null','Sample Product 10','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','10.00',11,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 11','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','18.79',22,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 12','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.','46.20',3,'sample_product1.jpg');

INSERT INTO Product VALUES('null','Sample Product 13','Sample','Sample product Description. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.',55.00,2,'sample_product2.jpg');
