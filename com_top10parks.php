<?php
/**
 * sh404SEF support for com_XXXXX component.
 * Author : S Attrill
 * contact : simon.attrill@hairylemon.co.nz
 * 
 * This is a sh404SEF plugin file.  
 * Reasons for use:
 * (1) If user is logged in (Park Administrator editing park content) then do not apply SEF urls 
 *     (to ensure eg cid[]=548 not replaced with cid,Array and stipping compoent from finding data).
 * (2) If user is not logged in then create SEF url for parks list page 'find-a-park'
 * (3) If user is not logged in then create SEF URL for park page (value of heading field in park table for parkid).
 *    
 * Hairy Lemon Modifications
 * =========================
 * Date        Who           Details
 * ----------- ------------- ---------------------------------------------------------------
 * 2010 AUG 02 S Attrill     Created.
 * 2010 AUG 06 D Noah        Fixed issue with front end Park Admin toolbar buttons not working. Replaced 'parks-list' with 'find-a-park'.
 */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

// ------------------  standard plugin initialize function - don't change ---------------------------
global $sh_LANG;
$sefConfig = & shRouter::shGetConfig();  
$shLangName = '';
$shLangIso = '';
$title = array();
$shItemidString = '';

$dosef = shInitializePlugin( $lang, $shLangName, $shLangIso, $option);
fb('starting initialsePlugin() said dosef='.$dosef,'SEF');
//If user is logged in (Park Administrator, then do not created SEF urls, eg so &cid[]=548 not translated to &cid,Array and item not found!)
$user =& JFactory::getUser();
if($user->id) {
	//fb("User is logged on so SEF is disabled.",'SEF');
}
// ------------------  standard plugin initialize function - don't change ---------------------------

// ------------------  load language file - adjust as needed ----------------------------------------
//$shLangIso = shLoadPluginLanguage( 'com_XXXXX', $shLangIso, '_SEF_SAMPLE_TEXT_STRING');
// ------------------  load language file - adjust as needed ----------------------------------------
// --- HLSA Ensure SEF Front End URLS
//SEF Parks List URL - Replace with "find-a-park" and remove variables
if (!$controller || $controller == 'frontend') {
	// $dosef = true;
	
	if (isset($view) && $view == 'top10parks') { 	// list parks
		  //Set SEF URL
		  $title[] = 'find-a-park';
		  //Remove non SEF parameters
		  if (isset($option)) shRemoveFromGETVarsList('option');
		  if (isset($view)) shRemoveFromGETVarsList('view');
	}

	// --- HLSA Ensure SEF Front End URLS
	//Individual Park Urls - Use Park Name 
	elseif (isset($view) && $view == 'frontend' && isset($parkid)) {

		// fb("Formatting park URL.","sef_ext\com_top10parks");
		// fb("Getting park name for parkid:".$parkid,"sef_ext\com_top10parks");
		// Get park name from latest park record.
		$q = 'SELECT heading FROM #__tt_park WHERE parkid = '.$parkid.' order by id DESC limit 1';
		$database->setQuery($q);
		$ParkDetails = $database->loadObject( );
		if ($ParkDetails) 
			{
			//Set SEF URL
			$title[] = 'parks';
			$title[] = strtolower($ParkDetails->heading);
			//Remove non SEF parameters
			if (isset($parkid)) shRemoveFromGETVarsList('parkid');
			if (isset($controller)) shRemoveFromGETVarsList('controller');
			if (isset($option)) shRemoveFromGETVarsList('option');
			if (isset($view)) shRemoveFromGETVarsList('view');
			}
	}
	else {	// unknown view this controller - turn sef off
			$dosef = false;

	}
}
// other controllers - frontendadmin typically
else {
	$dosef = true;
	
	//Set SEF URL
	if (isset($controller)) $title[] = $controller;
	if (isset($view)) 	$title[] = $view;
	else 			$title[] = 'default';
	
	// parkid
	if (isset($cid) && is_array($cid)) 	$title[] = $cid[0];
	
	//Remove non SEF parameters
	if (isset($parkid)) shRemoveFromGETVarsList('parkid');
	if (isset($controller)) shRemoveFromGETVarsList('controller');
	if (isset($option)) shRemoveFromGETVarsList('option');
	if (isset($view)) shRemoveFromGETVarsList('view');
	if (isset($task)) shRemoveFromGETVarsList('task');
	if (isset($cid)) shRemoveFromGETVarsList('cid');
}


if ($dosef) {
	// remove common URL from GET vars list, so that they don't show up as query string in the URL
	shRemoveFromGETVarsList('lang');
	
	if (!empty($Itemid))
		shRemoveFromGETVarsList('Itemid');
	 
	if (!empty($limit))  
	shRemoveFromGETVarsList('limit');
	if (isset($limitstart)) 
	  shRemoveFromGETVarsList('limitstart'); // limitstart can be zero
}

fb('finished : doing sef='.$dosef.' string='.$string,'SEF');
fb($title,'SEF');
// ------------------  standard plugin finalize function - don't change ---------------------------  
if ($dosef) {
   $string = shFinalizePlugin( $string, $title, $shAppendString, $shItemidString, 
	  (isset($limit) ? @$limit : null), (isset($limitstart) ? @$limitstart : null), 
	  (isset($shLangName) ? @$shLangName : null));
}

else {	// no sef rewriting needed this url...
	return;
}
// ------------------  standard plugin finalize function - don't change ---------------------------

?>
