<?php // HTML Header stuff ?>
<html>
<head>
<title>Sample - Send Email to David Script</title>

<?php //Script to validate the form (OPTIONAL)?>
<script language="javascript" type="text/javascript">
<!-- Hide from very old browsers than no one uses anymore 
// Check that a message has been entered
function checkForm(form1) {
if (form1.ccomment.value.length == 0) {
   form1.ccomment.value = alert("You forgot to enter your message!");
   form1.ccomment.value = '';
   form1.ccomment.focus();
   return false;
  }
// Check that message is not longer than 1000 characters
  if (form1.ccomment.value.length > 1000) {
   comment = (form1.ccomment.value);
   form1.ccomment.value = alert("Please enter a message less than 1000 charaters");
   form1.ccomment.value = comment;
   form1.ccomment.focus();
   return false;
  }
// Check that a name has been entered
  if (form1.cname.value.length == 0) {
   form1.cname.value = alert("You forgot to enter your name!");
   form1.cname.value = '';
   form1.cname.focus();
   return false;
  }
// Check that an from email address has been entered
  if (form1.cemail.value.length == 0) {
   form1.cemail.value = alert("You forgot to enter your Email address!");
   form1.cemail.value = '';
   form1.cemail.focus();
   return false;
  }
// If all the checks pass return true!!
  return true;
}
//-->
</script>
<?php // More html header crap ?>
</head>
<body bgcolor="#999999">

<?php
/**********************************************************************************
The following functions handle formatting and submiting the email message
Once the message is formatted correctly the Variables are sent to mail() which is
the mail function handled by php
***********************************************************************************/
//check that someone has submitted the form
if (array_key_exists('submitcontact', $_POST)) {

//pull variables out of form array (form)
$ccomment = $_POST['ccomment'];
$cname = $_POST['cname'];
$cemail = $_POST['cemail'];
$cuseragent = $_POST['cuseragent'];
$cremoteaddr = $_POST['cremoteaddr'];

// Filter out HTML code as this could be malisious
$ccomment = htmlspecialchars($ccomment);
$cname = htmlspecialchars($cname);
$cemail = htmlspecialchars($cemail);

// apply formatting to message eg paragraphs and line breaks
$ccomment = ereg_replace("\r","",$ccomment);
$ccomment = ereg_replace("\n\n","<p>",$ccomment);
$ccomment = ereg_replace("\n","<br>",$ccomment);
$ccomment = eregi_replace("  ","&nbsp;&nbsp;",$ccomment);


// Format mail headers and send form data as an email
$fromname = 'Website Form Email';
$fromemail = 'someone@whateveryouwant.co.nz';
$sendto = 'dmurch@athelas.no-ip.com';
$subject = 'Website Form Email';
// $msgbody is how you change the layout of the email as to where the data gets displayed
$msgbody = ("<b>Message </b><br>" . $ccomment . "<br><br> <b>Name:</b> " . $cname . "<br><b> Email: </b>" . $cemail . "<br><br><b> Web Browser</b><br>" . $cuseragent . "<br><b> IP Address</b><br>" . $cremoteaddr . "");
$headers  = "From: \"".$fromname."\" <".$fromemail.">\r\n"; 
$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n"; 
$headers .= "MIME-Version: 1.0\r\n"; 
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

// Core mail funtion that actually sends the message passing the above variables into it
$ok = mail($sendto, $subject, $msgbody, $headers);

// Error handling
if ($ok) {
echo("<center><h1>Thankyou. Your email as been sent.</h1></center>");
} else {
echo("<p>An Error has occured your email has not been sent. </p>");
}
}
?>

<?php
/******************************************************************************
HTML Form where the user enters the email message
This form calls a function to validate the form
The data is captured in variables that are the name"" part of each for m box
******************************************************************************/
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="form1" onSubmit="return checkForm(this)">
                    <table width="500" border="0" cellspacing="2" cellpadding="2" align="center">
                      <tr> 
                        <td colspan="2"><input type="hidden" name="cuseragent" value="<?php echo("" . $_SERVER['HTTP_USER_AGENT'] . ""); ?>"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><input type="hidden" name="cremoteaddr" value="<?php echo("" . $_SERVER['REMOTE_ADDR'] . ""); ?>"></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><p><strong>Write your email message here:</strong></p></td>
                      </tr>
                      <tr> 
                        <td width="130">&nbsp;</td>
                        <td><textarea name="ccomment" cols="35" rows="8" id="ccomment"></textarea></td>
                      </tr>
                      <tr> 
                        <td colspan="2"><p><strong>Your Contact Information:</strong></p></td>
                      </tr>
                      <tr> 
                        <td align="right" valign="top"> 
                          <p>Name</p></td>
                        <td><input name="cname" type="text" size="30" maxlength="30"></td>
                      </tr>
                      
                      
                      <tr> 
                        <td align="right" valign="top"> 
                          <p>E-mail</p></td>
                        <td><input name="cemail" type="text" size="30" maxlength="60"></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><input type="submit" name="submitcontact" value="Submit"> 
                        <input type="reset" name="Reset" value="Reset"> </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="top"><a href="https://athelasmail.no-ip.com/exchange">Check Email </a></td>
                      </tr>
                    </table>
</form>