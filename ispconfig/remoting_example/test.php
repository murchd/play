<?php

/**
* ISPConfig Soap Connector
* Version 1.3
* (c) Projektfarm GmbH 2005
*
* This script requires PHP with CURL extension
*
*/

include("soap.lib.php");

// Insert here your 42go Server
$server_url = "https://192.168.0.1:81";

// creating object for soap_client
$soap_client = new soap_client($server_url.'/remote/index.php');

// Username and Password of the remoting user (not identical
// with the user to log into the web interface!)

$parameters = array('user' => 'admin',
                    'pass' =>  'admin');

// Login into 42go Server
$session_id = $soap_client->call('login',$parameters);

// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);

/*
// Get Reseller List
$params = array (	'sid'       => $session_id,
                    'module'	=> 'reseller',
                    'function'	=> 'reseller_list',
                    'params'    => '');


$reseller_list = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($reseller_list);
*/

/*
// Get Reseller
$params = array (	'sid'        => $session_id,
                    'module'     => 'reseller',
                    'function'   => 'reseller_get',
                    'params'     => array ( reseller_title => "Reseller1"));


$reseller = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($reseller);
*/

/*
// Adding a reseller
$params = array ( 'sid'      => $session_id,
                  'module'   => 'reseller',
                  'function' => 'reseller_add',
                  'params'   => array (         reseller_title => 'Reseller1',
                                firma => 'Reseller4',
                                vorname => 'Jens',
                                limit_user => '50',
                                limit_disk => '1000',
                                limit_web => '10',
                                limit_domain => '10',
                                name => 'Jensen',
                                strasse => 'Hauptstr. 1',
                                plz => '12345',
                                ort => 'Hauptstadt',
                                telefon => '0511 5469766',
                                fax => '0511 9799655',
                                email => 'test@hostobserver.com',
                                internet => 'http://www.reseller4.tld',
                                reseller_user => 'reseller4',
                                reseller_passwort => 'huhu',
                                anrede => 'Herr',  // Herr, Frau, Firma
                                land => 'Deutschland',
                                limit_httpd_include => '1',
                                limit_dns_manager => '1',
                                limit_domain_dns => '50',
                                province => 'Niedersachsen',
                                limit_shell_access => '0',
                                limit_cgi => '1',
                                limit_php => '1',
                                limit_ssi => '1',
                                limit_ftp => '1',
                                limit_mysql => '1',
                                limit_ssl => '1',
                                limit_anonftp => '1',
                                limit_standard_cgis => '1',
                                limit_wap => '1',
                                limit_error_pages => '1',
                                limit_frontpage => '0',
                                limit_mysql_anzahl_dbs => '100',
                                limit_slave_dns => '50',
                                client_salutatory_email_sender_email => '',
                                client_salutatory_email_sender_name => '',
                                client_salutatory_email_bcc => '',
                                client_salutatory_email_subject => '',
                                client_salutatory_email_message => '',
                                standard_index => '',
                                user_standard_index => ''
                                ));

$reseller_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($reseller_id);
*/

/*
// Updating a reseller
$params = array (         'sid'        => $session_id,
                                        'module'         => 'reseller',
                                        'function'         => 'reseller_update',
                                        'params'        => array (         reseller_title => 'Reseller1',
                                firma => 'Reseller4',
                                vorname => 'Horst',
                                limit_user => '50',
                                limit_disk => '1000',
                                limit_web => '10',
                                limit_domain => '10',
                                name => 'Jensen',
                                strasse => 'Hauptstr. 1',
                                plz => '12345',
                                ort => 'Hauptstadt',
                                telefon => '0511 5469766',
                                fax => '0511 9799655',
                                email => 'test@hostobserver.com',
                                internet => 'http://www.reseller4.tld',
                                reseller_passwort => 'test',
                                anrede => 'Herr',  // Herr, Frau, Firma
                                land => 'Deutschland',
                                limit_httpd_include => '1',
                                limit_dns_manager => '1',
                                limit_domain_dns => '50',
                                province => 'Niedersachsen',
                                limit_shell_access => '0',
                                limit_cgi => '1',
                                limit_php => '1',
                                limit_ssi => '1',
                                limit_ftp => '1',
                                limit_mysql => '1',
                                limit_ssl => '1',
                                limit_anonftp => '1',
                                limit_standard_cgis => '1',
                                limit_wap => '1',
                                limit_error_pages => '1',
                                limit_frontpage => '0',
                                limit_mysql_anzahl_dbs => '100',
                                limit_slave_dns => '50',
                                client_salutatory_email_sender_email => '',
                                client_salutatory_email_sender_name => '',
                                client_salutatory_email_bcc => '',
                                client_salutatory_email_subject => '',
                                client_salutatory_email_message => '',
                                standard_index => '',
                                user_standard_index => ''
                                ));

$reseller = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($reseller);
*/

/*
// Suspending a reseller
$params = array (         'sid'        => $session_id,
                                        'module'         => 'reseller',
                                        'function'         => 'reseller_suspend',
                                        'params'        => array (         reseller_title => 'Reseller4'));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting a reseller
$params = array (         'sid'        => $session_id,
                                        'module'         => 'reseller',
                                        'function'         => 'reseller_delete',
                                        'params'        => array (         reseller_title => 'Reseller4'));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Slave List
$params = array (         'sid'        => $session_id,
                          'module'     => 'slave',
                          'function'   => 'slave_list',
                          'params'     => array( reseller_title => 'all')); // Reseller1, admin, all


$slaves = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($slaves);
*/

/*
// Get Slave Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_get',
                                        'params'        => array ( domain => "test2.de" // domain or slave_id
                                        ));


$slave = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($slave);
*/

/*
// Adding a Slave DNS Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_add',
                                        'params'        => array (         reseller_title => 'Reseller1', // reseller_title or reseller_group
                                domain => 'huheu.com',
                                master_ip => '192.168.0.100'));

$slave_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
echo $slave_id;
*/

/*
// Updating a Slave DNS Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_update',
                                        'params'        => array (         domain => 'huheu.com',  // domain or slave_id
                                master_ip => '192.168.0.101'));

$slave = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($slave);
*/

/*
// Suspending a Slave DNS Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_suspend',
                                        'params'        => array (         domain => 'huheu.com'  // domain or slave_id
                                        ));

if($soap_client->call('service',$params)) echo "Slave DNS Record is suspended!";
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending a Slave DNS Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_unsuspend',
                                        'params'        => array (         domain => 'huheu.com'  // domain or slave_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting a Slave DNS Record
// Suspend first!
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_suspend',
                                        'params'        => array (         domain => 'huheu.com'  // domain or slave_id
                                        ));

if($soap_client->call('service',$params)) echo "Slave DNS Record is suspended!";
if($err = $soap_client->getError()) die("Error: ".$err);
sleep(15);
if($err = $soap_client->getError()) die("Error: ".$err);
$params = array (         'sid'        => $session_id,
                                        'module'         => 'slave',
                                        'function'         => 'slave_delete',
                                        'params'        => array (         domain => 'huheu.com' // domain or slave_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Client List
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_list',
                                        'params'        => array( reseller_title => 'all')); // Reseller1, admin, all


$kunden = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($kunden);
*/

/*
// Get Client
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_get',
                                        'params'        => array ( kunde_title => "Kunde3"));


$kunde = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($kunde);
*/

/*
// Adding a client
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_add',
                                        'params'        => array (         reseller_title => 'Reseller1',
                                kunde_title => 'Kunde6',
                                kunde_anrede => 'Herr',  // Herr, Frau, Firma
                                kunde_firma => 'Kunde6',
                                kunde_vorname => 'Tom',
                                kunde_name => 'Sawyer',
                                kunde_strasse => 'Hauptstr. 1',
                                kunde_plz => '12345',
                                kunde_ort => 'Hauptstadt',
                                kunde_land => 'Deutschland',
                                kunde_telefon => '0511 5469766',
                                kunde_fax => '0511 9799655',
                                kunde_email => 'kunde6@hostobserver.com',
                                kunde_internet => 'http://www.kunde6.tld',
                                webadmin_user => 'kunde6',
                                webadmin_passwort => 'kunde6',
                                kunde_province => 'Niedersachsen'
                                ));

$kunde_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($kunde_id);
*/

/*
// Updating a client
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_update',
                                        'params'        => array (         kunde_title => 'Kunde6',
                                kunde_anrede => 'Herr',  // Herr, Frau, Firma
                                kunde_firma => 'Kunde6',
                                kunde_vorname => 'Huckleberry',
                                kunde_name => 'Finn',
                                kunde_strasse => 'Hauptstr. 1',
                                kunde_plz => '12345',
                                kunde_ort => 'Hauptstadt',
                                kunde_land => 'Deutschland',
                                kunde_telefon => '0511 5469766',
                                kunde_fax => '0511 9799655',
                                kunde_email => 'kunde6@hostobserver.com',
                                kunde_internet => 'http://www.kunde6.tld',
                                webadmin_user => 'kunde6',
                                webadmin_passwort => 'kunde6',
                                kunde_province => 'Niedersachsen'
                                ));

$kunde = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($kunde);
*/

/*
// Suspending a client
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_suspend',
                                        'params'        => array (         kunde_title => 'Kunde6'));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending a client
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_unsuspend',
                                        'params'        => array (         kunde_title => 'Kunde6'));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting a client
$params = array (         'sid'        => $session_id,
                                        'module'         => 'kunde',
                                        'function'         => 'kunde_delete',
                                        'params'        => array (         kunde_title => 'Kunde6'));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Web List
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_list',
                                        'params'        => array( reseller_title => 'all', // Reseller1, admin, all
                                        kunde_title => 'Kunde6' // kunde_title or kuunde_id (kunde_title/kunde_id overrides reseller_title)
                                        ));


$webs = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($webs);
*/

/*
// Get a Web
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_get',
                                        'params'        => array ( web_title => "test6.de"  // web_title or web_id
                                        ));


$web = $soap_client->call('service',$params);
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($web);
*/

/*
// Adding a Web
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_add',
                                        'params'        => array (         kunde_title => 'Kunde6', // reseller_title or reseller_group
                                web_title => 'test7.de',
                                web_host => 'www',
                                web_domain => 'test7.de',
                                web_ip => '192.168.0.110',
                                web_speicher => '100', // MB
                                web_dns => 0,
                                web_userlimit => 5,
                                web_domainlimit => 2,
                                web_shell => 0,
                                web_cgi => 1,
                                web_standard_cgi => 1,
                                web_php => 1,
                                web_php_safe_mode => 1,
					  web_ruby	=> 1,
					  web_python	=> 1,
                                web_ssi => 1,
                                web_ftp => 1,
                                web_frontpage => 0,
                                web_mysql => 1,
                                web_mysql_anzahl_dbs => 5,
                                web_ssl => 0,
                                web_anonftp => 0,
                                web_anonftplimit => 0,
                                web_wap => 1,
					  web_cron	=> 1,
                                web_individual_error_pages => 1
                                ));

$web_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
echo $web_id;
*/

/*
// Updating a Web
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_update',
                                        'params'        => array (         web_title => 'test7.de',
                                web_host => 'www',
                                web_domain => 'test7.de',
                                web_ip => '192.168.0.110',
                                web_speicher => '100', // MB
                                web_dns => 0,
                                web_userlimit => 5,
                                web_domainlimit => 2,
                                web_shell => 0,
                                web_cgi => 1,
                                web_standard_cgi => 1,
                                web_php => 1,
                                web_php_safe_mode => 1,
					  web_ruby	=> 1,
					  web_python	=> 1,
					  web_cron	=> 1,
                                web_ssi => 0,
                                web_ftp => 1,
                                web_frontpage => 0,
                                web_mysql => 1,
                                web_mysql_anzahl_dbs => 5,
                                web_ssl => 1,
                                web_anonftp => 0,
                                web_anonftplimit => 0,
                                web_wap => 1,
                                web_individual_error_pages => 1,
                                ssl_action => 'create', // create, save, delete
                                ssl_country => 'DE',
                                ssl_state => 'Niedersachsen',
                                ssl_locality => 'Lueneburg',
                                ssl_organization => 'Projektfarm GmbH',
                                ssl_organization_unit => 'IT',
                                ssl_days => 365,
                                web_dns_mx => 0,
                                web_httpd_include => '',
                                optionen_local_mailserver => 1,
                                web_faktura => 1,
                                optionen_logsize => '30%',
                                optionen_directory_index => 'index.html
index.htm
index.php
index.php4
index.php3
index.shtml
index.cgi
index.pl
index.jsp
Default.htm
default.htm',
                                error_400 => '',
                                error_401 => '',
                                error_403 => '',
                                error_404 => '',
                                error_405 => '',
                                error_500 => '',
                                error_503 => ''
                                ));

$web = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($web);
*/

/*
// Suspending a Web
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_suspend',
                                        'params'        => array (         web_title => 'test7.de'  // web_title or web_id
                                        ));

if($soap_client->call('service',$params)) echo "Web is suspended!";
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending a Web
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_unsuspend',
                                        'params'        => array (         web_title => 'test7.de'  // web_title or web_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting a Web
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'web_delete',
                                        'params'        => array (         web_title => 'test7.de'  // web_title or web_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// List DNS Records
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_list',
                                        'params'        => array (         reseller_title => 'all'  // Reseller1, admin, all
                                        ));


$dns_records = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($dns_records);
*/

/*
// Get a Zone
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_get',
                                        'params'        => array ( dns_soa => 'testdomain.com' // dns_soa or zone_id
                                        ));

$zone = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($zone);
*/

/*
// Adding a dns zone record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_add',
                                        'params'        => array ( reseller_title => 'admin', // reseller_title or reseller_id
                                        dns_soa => 'test7.de',
                                        dns_soa_ip => '192.168.0.110',
                                        dns_refresh => 28800,
                                        dns_retry => 7200,
                                        dns_expire => 604800,
                                        dns_ttl => 86400,
                                        dns_ns1 => 'ns1.providerdomain.com',
                                        dns_ns2 => 'ns2.providerdomain.com',
                                        dns_adminmail => 'admin@test7.de'
                                        ));

$zone_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
echo $zone_id;
*/

/*
// Updating a dns zone record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_update',
                                        'params'        => array ( dns_soa => 'test7.de',
                                        dns_soa_ip => '192.168.0.111',
                                        dns_refresh => 28800,
                                        dns_retry => 7300,
                                        dns_expire => 604800,
                                        dns_ttl => 86400,
                                        dns_ns1 => 'ns1.providerdomain.com',
                                        dns_ns2 => 'ns2.providerdomain.com',
                                        dns_adminmail => 'admin@test7.de'
                                        ));

$zone = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($zone);
*/

/*
// Suspending a dns zone record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_suspend',
                                        'params'        => array ( dns_soa => 'test7.de' // dns_soa or zone_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending a dns zone record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_unsuspend',
                                        'params'        => array ( dns_soa => 'test7.de' // dns_soa or zone_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting a dns zone record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'zone_delete',
                                        'params'        => array ( dns_soa => 'test7.de' // dns_soa or zone_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// List A Records
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_list',
                                        'params'        => array ( dns_soa => 'test7.de' // dns_soa or zone_id
                                        ));

$as = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($as);
*/

/*
// Get an A Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_get',
                                        'params'        => array ( dns_soa => 'test7.de', // dns_soa or zone_id
                                        host => 'www' // host or a_id
                                        ));

$a = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($a);
*/

/*
// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// adding A-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_add',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2',
                                                                                        ip_adresse => '192.168.0.1'));

$a_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($a_id);
*/

/*
// Updating A-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_update',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2', // host or a_id
                                                                                        ip_adresse => '192.168.0.2'));

$a = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($a);
*/

/*
// Suspending A-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_suspend',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2' // host or a_id
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending A-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_unsuspend',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2' // host or a_id
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting A-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'a_delete',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2' // host or a_id
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// List CNAME Records
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_list',
                                        'params'        => array ( dns_soa => 'test7.de' // dns_soa or zone_id
                                        ));

$cnames = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($cnames);
*/

/*
// Get CNAME Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_get',
                                        'params'        => array ( dns_soa => 'test7.de', // dns_soa or zone_id
                                        host => 'huhu' // host or cname_id
                                        ));

$cname = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($cname);
*/

/*
// adding CNAME-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_add',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2',
                                                                                        ziel => 'www.test7.de'));

$cname_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($cname_id);
*/

/*
// Updating CNAME-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_update',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2', // host or cname_id
                                                                                        ziel => 'www.test8.de'));

$cname = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($cname);
*/

/*
// Suspending CNAME-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_suspend',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2', // host or cname_id
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending CNAME-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_unsuspend',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2', // host or cname_id
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting CNAME-Record "www2"
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'cname_delete',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'www2', // host or cname_id
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// List MX Records
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_list',
                                        'params'        => array ( dns_soa => 'test7.de' // dns_soa or zone_id
                                        ));

$mxs = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($mxs);
*/

/*
// Get MX Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_get',
                                        'params'        => array ( dns_soa => 'test7.de', // dns_soa or zone_id
                                        mx_id => 4 // only mx_id!!!
                                        ));

$mx = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($mx);
*/

/*
// adding MX-Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_add',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        host =>        'hgfhfhf',
                                                                                        prioritaet => '20',
                                                                                        mailserver => 'mail.mail.com'
                                                                                        ));

$mx_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($mx_id);
*/

/*
// Updating MX-Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_update',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        mx_id => 4, // only mx_id!!!
                                                                                        host =>        'blubb',
                                                                                        prioritaet => '40',
                                                                                        mailserver => 'mail.mail.com'
                                                                                        ));

$mx = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($mx);
*/

/*
// Suspending MX-Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_suspend',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        mx_id => 4 // only mx_id!!!
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspending MX-Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_unsuspend',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        mx_id => 4 // only mx_id!!!
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Deleting MX-Record
$params = array (         'sid'        => $session_id,
                                        'module'         => 'dns',
                                        'function'         => 'mx_delete',
                                        'params'        => array(         dns_soa => 'test7.de', // dns_soa or zone_id
                                                                                        mx_id => 4 // only mx_id!!!
                                                                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// User List
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_list',
                                        'params'        => array ( web_title => 'test5.de' // web_title or web_id
                                        ));

$users = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($users);
*/

/*
// Get a User
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_get',
                                        'params'        => array ( user_username => 'web21_heinz' // user_username or user_id
                                        ));

$user = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($user);
*/

/*
// Add a User
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_add',
                                        'params'        => array ( web_title => 'test7.de',  // web_title or web_id
                                        user_username => 'web21_uwe',
                                        user_name => 'Uwe Meier',
                                        user_email => 'uwe',
                                        user_passwort => 'sagichnicht',
                                        user_speicher => 10,
                                        user_mailquota => 10,
						    user_shell	=> 1,
						    user_cron	=> 1,
                                        user_admin => 1
                                        ));

$user_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($user_id);
*/

/*
// Update a User
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_update',
                                        'params'        => array ( user_username => 'web21_uwe', // user_username or user_id
                                        user_passwort => 'sagichnicht',
                                        user_speicher => 10,
                                        user_admin => 0,
						    user_cron	=> 0,
                                        user_shell => '0',
                                        user_emailalias => "karl-heinz\notto",
                                        user_name => 'Uwe Meier',
                                        user_autoresponder => 0,
                                        user_autoresponder_text => '',
                                        user_mailquota => 10,
                                        user_catchallemail => 0,
                                        user_mailscan => 0,
                                        user_emailweiterleitung_local_copy => 1,
                                        user_email => 'uwe',
                                        user_emailweiterleitung => 'uwe@t-online.de',
                                        user_spamfilter => 0,
                                        spam_strategy => 'accept',
                                        spam_hits => '5.0',
                                        spam_rewrite_subject => 1,
                                        spam_subject_tag => '*****SPAM***** ',
                                        antivirus => 0
                                        ));

$user = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($user);
*/

/*
// Suspend a User
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_suspend',
                                        'params'        => array ( user_username => 'web21_uwe' // user_username or user_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspend a User
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_unsuspend',
                                        'params'        => array ( user_username => 'web21_uwe' // user_username or user_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Delete a User
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'user_delete',
                                        'params'        => array ( user_username => 'web21_uwe' // user_username or user_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Database List
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_list',
                                        'params'        => array ( web_title => 'test1.de' // web_title or web_id
                                        ));

$datenbanken = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($datenbanken);
*/

/*
// Get a Database
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_get',
                                        'params'        => array ( datenbankname => 'web1_db1' // datenbankname or datenbank_id
                                        ));

$datenbank = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($datenbank);
*/

/*
// Add a Database
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_add',
                                        'params'        => array ( web_title => 'test1.de',  // web_title or web_id
                                        db_passwort => 'hallo',
                                        remote_access => '1'
                                        ));

$datenbank_id = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($datenbank_id);
*/

/*
// Update a Database
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_update',
                                        'params'        => array ( datenbankname => 'web1_db3',  // datenbankname or datenbank_id
                                        db_passwort => 'geheim',
                                        remote_access => '0'
                                        ));

$datenbank = $soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
print_r($datenbank);
*/

/*
// Suspend a Database
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_suspend',
                                        'params'        => array ( datenbankname => 'web1_db3' // datenbankname or datenbank_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Unsuspend a Database
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_unsuspend',
                                        'params'        => array ( datenbankname => 'web1_db3' // datenbankname or datenbank_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

/*
// Delete a Database
$params = array (         'sid'        => $session_id,
                                        'module'         => 'web',
                                        'function'         => 'datenbank_delete',
                                        'params'        => array ( datenbankname => 'web1_db3' // datenbankname or datenbank_id
                                        ));

$soap_client->call('service',$params);
if($err = $soap_client->getError()) die("Error: ".$err);
*/

// 42go Server logout
$soap_client->call('logout',array('sid' => $session_id));

// Error Check
if($err = $soap_client->getError()) die("Error: ".$err);

echo "<br>Script end ...";



?>