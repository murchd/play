
Installation of ISPCOnfig Remoting-Framework Test scripts
---------------------------------------------------------

ISPCOnfig 2.1.2 or higher required.

1) Install the remoting extension in ISPConfig.


2) Add a new remoting user in your ISPConfig.
   The remoting extension adds a new item in
   Tools > Remoting > User.

   The IP address is optional. If the IP address is set,
   this user can use the remoting functions only from
   this IP address.

   Then check all checkboxes for functions that shall be
   available for the user.
 

3) Copy the php scripts to a php enabled Webserver.
   PHP must be compiled with the CURL extension, otherwise
   it wont work!

4) Edit the test.php script

  - change the server URL on line 15
  - change username and password on line 23 / 24. Put there the
    username and password of the remoting user added in step 2.
  - Uncomment one of the functions below.

5) call the test.php script in your browser.


