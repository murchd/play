<?php
$proxy = new SoapClient('http://magento.local/index.php/api?wsdl');
// If soap isn't default use this link instead
// http://youmagentohost/api/soap/?wsdl

// If somestuff requires api authentification,
// we should get session token
$sessionId = $proxy->login('remote', 'qwerty12345');

$attributeSets = $proxy->call($sessionId, 'product_attribute_set.list');
$set = current($attributeSets);
 
 
$newProductData = array(
    'name'              => 'my product from soap client',
     // websites - Array of website ids to which you want to assign a new product
    'websites'          => array(1), // array(1,2,3,...)
    'short_description' => 'a simple description',
    'description'       => 'longer descipriotn',
    'status'            => 1,
    'weight'            => 0,
    'tax_class_id'      => 1,
    'categories'    => array(3),    //3 is the category id   
    'price'             => 45.99
);
 
// Create new product
$proxy->call($sessionId, 'product.create', array('simple', $set['set_id'], '14424545121', $newProductData));

$result = $proxy->call($sessionId, 'product.list');
print_r($result);



// If you don't need the session anymore
$proxy->endSession($session);
?>