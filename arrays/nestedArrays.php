<?php
$arrayCategories = array(
					'1'=>array('Name'=>'Security'),
					'2'=>array('Name'=>'Cameras'),
					'3'=>array('Name'=>'Home'),
					'4'=>array('Name'=>'Audio'),
					'5'=>array('Name'=>'Head Units'),
					'6'=>array('Name'=>'GPS')
				);
$arrayProducts = array(
					'1'=>array('Title'=>'Product1',
								'Desc'=>'The Quick brown fox',
								'Price'=>'56.12',
								'category'=>'3'),
					'2'=>array('Title'=>'Product2',
								'Desc'=>'The Quick brown fox',
								'Price'=>'56.12',
								'category'=>'6'),
					'3'=>array('Title'=>'Product3',
								'Desc'=>'The Quick brown fox',
								'Price'=>'56.12',
								'category'=>'4'),
					'4'=>array('Title'=>'Product4',
								'Desc'=>'The Quick brown fox',
								'Price'=>'56.12',
								'category'=>'2'),
					'5'=>array('Title'=>'Product5',
								'Desc'=>'The Quick brown fox',
								'Price'=>'56.12',
								'category'=>'1')
				);
				
				//print_r($arrayCategories);
				//print_r($arrayProducts);
				
foreach($arrayCategories as $categoryKey => $category) {
	echo $category['Name'].'<br><br>';
	foreach($arrayProducts as $product) {
		if($product['category'] == $categoryKey) {
			echo '<blockquote>'.$product['Title'].'<br></blockquote>';
		}
	}
	echo '<br>';
}
?>