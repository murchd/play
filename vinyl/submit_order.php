
<?php

//check that someone has submitted the form
if (array_key_exists('submit_order', $_POST)) {
//session_start();


	// Format mail headers and send form data as an email
	$fromname = 'Vinyl Product Order';
	$fromemail = 'noreply@noreply.com';
	$sendto = 'david@davidmurch.com';
	//$sendto = 'david@davidmurch.com';
	$subject = 'Vinyl Product Order';
	// $msgbody is how you change the layout of the email as to where the data gets displayed
	$msgbody = ("<font size=2>This email has been sent from an online form</font><br><br>");
	$msgbody .= ("<b><u>Order Details </u></b><br><br>");
	$msgbody .= ("" . $ok . "<br><br>");
	
	$headers  = "From: \"".$fromname."\" <".$fromemail.">\r\n"; 
	$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n"; 
	$headers .= "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 

	// Core mail funtion that actually sends the message passing the above variables into it
		$ok = mail($sendto, $subject, $msgbody, $headers);

	// Error handling
	if ($ok) {
		echo("<p><b>Thankyou.</b> Your order has been sent successfully. You will be contacted via the email address provided regarding payment and shipping.</p>");
	} else {
		echo("<p>An Error has occured your order has not been sent. </p>");
	}

} else {
	include 'submit_orderForm.php';
}
?>