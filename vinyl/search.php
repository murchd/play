<?php
session_start();
	 if (array_key_exists('searchstring', $_POST)) {
	 	@session_unregister("searchby");
		@session_unregister("searchstring");
	 	$searchby = 0;
		$searchstring = 0;

	 	$searchby = $_POST["searchby"];
		$searchstring = $_POST["searchstring"];
	}
		session_register("searchby");
		session_register("searchstring");

?>
<link href="stylesheet.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<h1>Search the vinyl Database</h1>
<p>Search by ... (select multiple)</p>
<form id="form1" name="form1" method="post" action="<?php echo $PHP_SELF; ?>" onSubmit="return checkForm(this)" enctype="multipart/form-data">


  <select name="searchby[]" size="4" multiple="multiple">
  	<option value="artist">Artist</option>
	<option value="upc">UPC</option>
	<option value="title">Title</option>
	<option value="config">Config</option>
  </select>
	
	
	<br><br>
	
	<p>For text containing ... </p>
	<?php $searchstring = stripslashes($searchstring);?>
  <input type="text" name="searchstring" value=<?php echo $searchstring; ?>>
  
  <br><br>
  
  <input type="submit" name="search" value="Search"/>

</form>




<?php
	// if (array_key_exists('searchstring', $_POST)) {

		if(empty($searchby) or empty($searchstring)) {
			echo "<p>Please enter a search string and a field to search by</p>";
		} else {
			$thedatabase = 'dwm77_db';
	 		include 'dbconn.inc.php';
			$sql = 'select * from vinyl where ';
			$count = 0;
			foreach ($searchby as $value) {
				if ($count >= 1) {
					$sql .= "AND ";
				}
				$count ++;
				$sql .= "$value LIKE '%$searchstring%' ";
			}

			// how many rows to show per page 
				$rowsPerPage = 20; 
				
				// by default we show first page 
				$pageNum = 1; 
				
				// if $_GET['page'] defined, use it as page number 
				if(isset($_GET['page'])) 
				{ 
					$pageNum = $_GET['page']; 
				} 
				
				// counting the offset 
				$offset = ($pageNum - 1) * $rowsPerPage; 
				//query the database
				$counterquery  = $sql; 
				$resultcounter = mysql_query($counterquery) or die('Error, query failed'); 
				$totalRecords = mysql_num_rows($resultcounter);
				$pagestart = $offset + 1;
				$pageend = $offset + $rowsPerPage;
				if ($pageend >= $totalRecords) {
				$diff = ($pageend - $totalRecords);
				$pageend =($pageend - $diff);
				}
				$sql .= "LIMIT $offset, $rowsPerPage";
			//echo $sql;
			echo '<br>';
			$result = mysql_query($sql);
			if(mysql_error()) {
			 	echo mysql_error() ."<br>\n";
		  	} else {
				//echo '<br><p>Query OK...</p><br>';
				
				$output = ("<table border width=600>" .
						"<tr>" .
							"<td colspan=5><font style='font-family: verdana; font-size: 9pt; font-weight=700'>Showing " . $pagestart . " to " . $pageend . " of " . $totalRecords . " records returned</font></td>" .
						"</tr>" .
						"<tr>" .
							"<td><b>UPC</b></td><td><b>Artist</b></td><td><b>Title</b></td><td><b>Config</b></td><td><b>Add</b></td>" .
						"</tr>");
				$count = 0;
				while ( $row = mysql_fetch_array($result) ) {
					$upc = $row["UPC"];
					$artist = $row["artist"];
					$title = $row["title"];
					$config = $row["config"];
					$output .=("<tr><td>".$upc."</td><td>".$artist."</td><td>".$title."</td><td>".$config."</td><td><form action=cart.php target=rightFrame method=POST name=form$upc><input type=hidden name=upc value=".$upc."><input type=hidden name=qty value=1><input type=submit value='Add to Cart' name=add></form></td></tr>");
					$count ++;
				}
				$output .= ("</table>");
				//echo ("".$count." records returned");
				if($count > 0) {
					echo $output;
					// how many rows we have in database 
					$numrows = $totalRecords; 
					
					// how many pages we have when using paging? 
					$maxPage = ceil($numrows/$rowsPerPage); 
					
					$self = $_SERVER['PHP_SELF']; 
					
					// creating 'previous' and 'next' link 
					// plus 'first page' and 'last page' link 
					
					// print 'previous' link only if we're not 
					// on page one 
					if ($pageNum > 1) 
					{ 
						$page = $pageNum - 1; 
						$prev = " <a href=\"$self?page=$page\">[Prev]</a> "; 
						 
						$first = " <a href=\"$self?page=1\">[First Page]</a> "; 
					} 
					else 
					{ 
						$prev  = '<font color=#999999> [Prev] </font>';       // we're on page one, don't enable 'previous' link 
						$first = '<font color=#999999> [First Page]</font> '; // nor 'first page' link 
					} 
					
					// print 'next' link only if we're not 
					// on the last page 
					if ($pageNum < $maxPage) 
					{ 
						$page = $pageNum + 1; 
						$next = " <a href=\"$self?page=$page\">[Next]</a> "; 
						 
						$last = " <a href=\"$self?page=$maxPage\">[Last Page]</a> "; 
					} 
					else 
					{ 
						$next = ' <font color=#999999>[Next] </font>';      // we're on the last page, don't enable 'next' link 
						$last = ' <font color=#999999>[Last Page] </font>'; // nor 'last page' link 
					} 
					
					// print the page navigation link 
					echo ("<font style='font-family: verdana; font-size: 9pt; font-weight=700'>" . $first . $prev . " Showing page <strong>" . $pageNum . "</strong> of <strong>" . $maxPage . "</strong> pages" . $next . $last. "</font>"); 
				}
			}
		
		}
		
		
//	}
		
		
		
		
?>
