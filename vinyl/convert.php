<script language="javascript" type="text/javascript">
<!-- Hide from very old browsers than no one uses anymore 
// Check that a message has been entered
function checkForm(form1) {
if (form1.theconfig.value.length == 0) {
   form1.theconfig.value = alert("Please enter the config");
   form1.theconfig.value = '';
   form1.theconfig.focus();
   return false;
  }
// If all the checks pass return true!!
  return true;
}
//-->
</script>
<h1>Convert tab delimited .txt into SQL and insert into MySQL DB</h1>
<p><strong>By David Murch</strong></p>
<!-- 12/01/06 -->
<form id="form1" name="form1" method="post" action="<?php echo $PHP_SELF; ?>" onSubmit="return checkForm(this)" >
  <p>Please enter the database details below</p>
  <table width="600">
  <tr>
  <td bgcolor="#CCCCCC">
 The database Username </td><td bgcolor="#CCCCCC"><input type="text" name="theusername" value="sampleuser"/><br>
 </td>
 </tr>
 <tr>
 <td bgcolor="#CCCCCC">
  The database Password</td><td bgcolor="#CCCCCC"> <input type="password" name="thepassword"  value="samplepassword"/><br>
   </td>
 </tr>
 <tr>
 <td bgcolor="#CCCCCC">
  The Database name</td><td bgcolor="#CCCCCC"> <input type="text" name="thedatabase"  value="dwm77_db"/><br>
   </td>
 </tr>
 <tr>
 <td bgcolor="#CCCCCC">
    The table name </td><td bgcolor="#CCCCCC"><input type="text" name="thetablename"  value="vinyl"/><br>
	 </td>
 </tr>
 <tr>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
 </tr>
 <tr>
   <td>Please enter the data to be inserted below </td>
   <td>&nbsp;</td>
 </tr>
 <tr>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
 </tr>
 <tr>
 <td bgcolor="#CCCCCC"><?php $theconfig = $_POST["theconfig"]; $theconfig = stripslashes($theconfig);?>
    The config </td><td bgcolor="#CCCCCC"><input type="text" name="theconfig"  value=<?=$theconfig;?>><br>
	 </td>
 </tr>
 <tr>
   <td bgcolor="#CCCCCC">&nbsp;</td>
   <td bgcolor="#CCCCCC">&nbsp;</td>
 </tr>
 <tr>
   <td bgcolor="#CCCCCC"><strong>Either... (enter one row) </strong></td>
   <td bgcolor="#CCCCCC">&nbsp;</td>
 </tr>
 <tr>
   <td bgcolor="#CCCCCC">Enter the UPC, artist or title </td>
   <td bgcolor="#CCCCCC">&nbsp;</td>
 </tr>
 <tr>
   <td colspan="2" bgcolor="#CCCCCC">UPC: <input type="text" name="upc"  value=""/> 
     Artist: 
       <input type="text" name="artist"  value=""/>
     T
     itle: 
      <input type="text" name="title"  value=""/></td>
   </tr>
 <tr>
   <td colspan="2" bgcolor="#CCCCCC"><strong>Or ... (enter a whole file) </strong></td>
 </tr>
 <tr>
   <td colspan="2" bgcolor="#CCCCCC">Enter the file name (.txt) that is to be inserted. ( This
     file must be in the same folder as this script)</td>
 </tr>
 <tr>
   <td colspan="2" bgcolor="#CCCCCC">The Filename
     <input type="text" name="thefilename"  value="sample.txt"/></td>
 </tr>
 <tr>
   <td colspan="2">&nbsp;</td>
 </tr>
 </table>
 
  
  <input type="submit" name="submitcon" value="Insert"> &nbsp;<input type="submit" name="create" value="Create table">&nbsp;<input type="submit" name="drop" value="Drop table (WARNING!!)">
</form>


<?
	 if (array_key_exists('submitcon', $_POST)) {
	 	$theusername = $_POST["theusername"];
		$thepassword = $_POST["thepassword"];
		$thedatabase = $_POST["thedatabase"];
		$thefilename = $_POST["thefilename"];
		$thetablename = $_POST["thetablename"];
		$theconfig = $_POST["theconfig"];
		$theupc = $_POST["upc"];
		$theartist = $_POST["artist"];
		$thetitle = $_POST["title"];
		//$theconfig = htmlentities($theconfig);
		include 'dbconn.inc.php';
		if($theupc != '' && $theartist != '' && $thetitle != '') {
			$sql = "insert into $thetablename values ($theupc,'$theartist','$thetitle','$theconfig')";
			 if(mysql_query($sql)) {
			 	echo "Inserted OK ...";
				$nonfile = true;
			} else {
				echo "Query failed<br>".$sql."";
			}
		} else {
	   	# first get a mysql connection as per the FAQ
		$file = ("./".$thefilename."");
	  	$fcontents = @file ($file); 
		//echo $fcontents;
	 	# expects the csv file to be in the same dir as this script
			$count = 1;
			$okcount = 0;
			$errcount = 0;
	 	 for($i=0; $i<sizeof($fcontents); $i++) { 
			  $line = trim($fcontents[$i]); 
			  $line = addslashes($line);
			  $arr = explode("\t", $line); 
			  //echo $line.'<br>';
			  #if your data is comma separated
			  # instead of tab separated, 
			  # change the '\t' above to ',' 
			 
			  $sql = "insert into $thetablename values ('".implode("','", $arr)."','$theconfig')"; 
			  if(mysql_query($sql)) {
			  	//echo $sql ."<br>\n";
			  	if(mysql_error()) {
					echo $sql ."<br>\n";
			 		echo mysql_error() ." at record $count<br>\n";
		  		} else {
					if($count == 1) {
						$ok = "inserted OK ... Errors listed below if any</b></p>";
					} 
					$okcount ++;
					$count ++;
					//echo '<br><p><strong>Convert and insert completed YAY!!!!!</strong></p><br>';
				}
			} else {
				if(empty($thefilename)) {
					echo "<p>Please enter a file name (.txt)</p>";
				} else {
					$errcount ++;
					if($errcount == 1) {
					$erroutput .= 'Errors Found <br>';
					}
					$eout .= '<p>Query failed<br>'.mysql_error().' in SQL statement and at line '.$count.' in '.$thefilename.'</p>';
					$count ++;
					$eout .= $sql ."<br>\n";
					$eout .= "<hr>";
				}
			}
			}
		}
		if($nonfile == true) {
		
		
		} else {
		$erroutput .= 'Found '.$errcount.' errors';
		echo "<p><b>".$okcount." records ";
		echo $ok;
		echo $erroutput;
		echo $eout;
		}
	}
	
		if (array_key_exists('drop', $_POST)) {
			$theusername = $_POST["theusername"];
			$thepassword = $_POST["thepassword"];
			$thedatabase = $_POST["thedatabase"];
			$thetablename = $_POST["thetablename"];
			include 'dbconn.inc.php';
			$sql = "drop table $thetablename";
			mysql_query($sql);
			if(mysql_error()) {
			 	echo mysql_error() ."<br>\n";
		  	} else {
				echo '<br><p><strong>Table '.$thetablename .' was dropped!</strong></p><br>';
			}
		}
		
		if (array_key_exists('create', $_POST)) {
			$theusername = $_POST["theusername"];
			$thepassword = $_POST["thepassword"];
			$thedatabase = $_POST["thedatabase"];
			$thetablename = $_POST["thetablename"];
			include 'dbconn.inc.php';
			$sql = "create table $thetablename(UPC CHAR(20), artist CHAR(255), title CHAR(255), config CHAR(50))";
			mysql_query($sql);
			if(mysql_error()) {
			 	echo mysql_error() ."<br>\n";
		  	} else {
				echo '<br><p><strong>Table '.$thetablename .' was created!</strong></p><br>';
			}
		}
?>
