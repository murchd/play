<?php
header("Content-type: image/png");
$image = imagecreate(400, 55);
imagealphablending($image, True);
//$black = ImageColorAllocatealpha($image, 0, 0, 0, 64);
//$white = ImageColorAllocatealpha($image, 255, 255, 255, 70);
$white = imagecolorallocate($image, 255, 255, 255);
$black = imagecolorallocate($image, 74, 74, 74);
$bg_color = imagecolorat($image,1,1);
imagecolortransparent($image, $bg_color);
imagefilledrectangle($image, 0, 0, 200, 200);
$font = 'another.ttf';
imagettftext($image, 36, 0, 10, 40, $black, $font, "paintings");
imagepng($image);
imagedestroy($image);
?>