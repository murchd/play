<?php
require_once 'classes.php';

// simple string add and fetch
$first = new myfirst();
$first->addString('World');
$first->addString('My name is David');
echo $first->get();


// color match
$color = 'blue';
$filter = new ColorFilter($color);
if ($filter->isPrimaryColor() ) {
    echo ($color.' is a primary color<br>');
} else {
    echo ($color.' is not a primary color<br>');
}



$msg = &new Goodbye();
echo $msg->sayHello() . '<br />';
echo $msg->sayGoodbye() . '<br />';


$db = new MySQL();
$sql = "select * from customer";
$result = $db->query($sql);
echo $db->rowcount($result);
while($row = $db->fetch($result)) {
	echo $row['username'];
}


?>

