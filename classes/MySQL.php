<?php
/*
   MySQL Database Connection Class
*/

class MySQL 
{
  var  $host;
  var  $dbUser;
  var  $dbPass;
  var  $dbName;
  var  $dbConn;
  var  $dbconnectError;


  function MySQL ()
  {
     $this->host   = 'localhost';
     $this->dbUser = 'root';
     $this->dbPass = '';
     $this->dbName = 'nzatoz_db';
     $this->connectToDb();
   }


  function connectToDB()
  {
       if(!$this->dbConn = @mysql_connect($this->host , $this->dbUser , $this->dbPass)) {
          trigger_error ('could not connect to server' ) ;
          $this->dbconnectError = true ;
       echo $this->dbName;
	   }
       else if (!@mysql_select_db ( $this->dbName , $this->dbConn) )
       {
          trigger_error ('could not select database' ) ;  
          $this->dbconnectError = true ;                     
       }
  }


   function isError()
   {
      if  ( $this->dbconnectError )
       {
         return true ;
       }
       $error = mysql_error ( $this->dbConn ) ;
       if (empty ($error))
         {
           return false ;
         }
         else
         {
           return true ;   
         }

   }


   function &query ( $sql )
   {
     if (!$queryResource = mysql_query ( $sql , $this->dbConn ))
     {
        trigger_error ( 'Query Failed: ' . mysql_error ($this->dbConn ) . ' SQL: ' . $sql ) ;
     }
 
     return new MySQLResult( $this, $queryResource ) ; 
   }
}


class MySQLResult 
{
   var $mysql ;
   var $query ;

   function MYSQLResult ( &$mysql , $query )
   {
     $this->mysql = &$mysql ;
     $this->query = $query  ;
   }

    function size()
    {
      return mysql_num_rows($this->query) ;
    }

    function fetch()
    {
       if ( $row = mysql_fetch_array ( $this->query , MYSQL_ASSOC ))
       {

         return $row ;
       }

       else if ( $this->size() > 0 )
       {
         mysql_data_seek ( $this->query , 0 ) ;
         return false ;
       }
       else
       {
         return false ;
       }         

    }

   function isError()
    {
      return $this->mysql->isError() ;
    }


}



?>