<?php
	$image = $_GET['src'];
	
	//Your Image
	   $imgSrc = $image; 
	                     
	//getting the image dimensions
	   list($width, $height) = getimagesize($imgSrc); 
	                     
	//create image from the jpeg
	   $myImage = imagecreatefromjpeg($imgSrc) or die("Error: Cannot find image!"); 
	            
	       if($width > $height) $biggestSide = $width; //find biggest length
	       else $biggestSide = $height; 
	                     
	//The crop size will be half that of the largest side 
	   $cropPercent = .5; // This will zoom in to 50% zoom (crop)
	   $cropWidth   = $biggestSide*$cropPercent; 
	   $cropHeight  = $biggestSide*$cropPercent; 
	                     
	                     
	//getting the top left coordinate
	   $x = ($width-$cropWidth)/2;
	   $y = ($height-$cropHeight)/2;
	             

                    
  	$thumbSize = 50; // will create a 250 x 250 thumb
  	$thumb = imagecreatetruecolor($thumbSize, $thumbSize); 

  	imagecopyresampled($thumb, $myImage, 0, 0,$x, $y, $thumbSize, $thumbSize, $cropWidth, $cropHeight); 
             
   header('Content-type: image/jpeg');
   imagejpeg($thumb);
   imagedestroy($thumb); 





?>