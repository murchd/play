<?php
/*
 * This file shows how objects in PHP are passed by reference.
 *  Just a pointer to customer is passed to insertCustomer.
 *  The customer object in the mainline code contains all changes to the object
 */
class Customer {
	var $name;
	var $id;

}
$customer = new Customer();
$customer->name = "Tim";
$customer->email = "david.murch@hairylemon.co.nz";

insertCustomer($customer);

echo $customer->name."\r\n";
echo $customer->id."\r\n";
echo $customer->email;


function insertCustomer($var) {
	$var->name = "David";
	$var->id = "45";
}

?>


