<?php
// Component
interface MenuItem{
	public function cost();
}
 
// Concrete Component
class Coffee implements MenuItem{
	private $_cost = 1.00;
 
	public function cost(){
		return $this->_cost;
	}
 
}
 
// Decorator
abstract class MenuDecorator extends Coffee{
	protected $_component;
 
	public function __construct(MenuItem $component){
 
		$this->_component = $component;
 
	}
 
}
 
// Concrete Decorator
class CoffeeMilkDecorator extends MenuDecorator{
 
	public function cost(){
 
		return $this->_component->cost() + 0.5;
 
	}
 
}
 
// Concrete Decorator
class CoffeeWhipDecorator extends MenuDecorator{
 
	public function cost(){
 
		return $this->_component->cost() + 0.7;
 
	}
 
}
 
// Concrete Decorator
class CoffeeSprinklesDecorator extends MenuDecorator{
 
	public function cost(){
 
		return $this->_component->cost() + 0.2;
 
	}
 
}
 
 
// Example 1   
$coffee = new CoffeeMilkDecorator(new Coffee());
print "Coffee with milk : ".$coffee->cost();
 
// Example 2
$coffee = new CoffeeWhipDecorator(new CoffeeMilkDecorator(new CoffeeSprinklesDecorator(new Coffee())));
print "Coffee with milk, whip and sprinkles : ".$coffee->cost();
?>